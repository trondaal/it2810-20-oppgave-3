var express = require('express');
var path = require('path');
var server = express();

// Parsers ans sessions
var body_parser = require('body-parser');
var cookie_parser = require('cookie-parser');

// Query-handler-functions
var button_pushed = require('../queries/query_receivers/button_pushed');
var retrieve = require('../queries/query_receivers/retrieve');
var post_form = require('../queries/query_receivers/post_form');

// Global vars
var global_vars_private = require('../global_vars_private.js');
var global_vars_public = require('../global_vars_public.js');
var PORT = global_vars_public.PORT;
var IS_PROD = global_vars_public.IS_PROD;
var IS_DEV = global_vars_public.IS_DEV;

// User
var models_and_model_settings = require('../models/models_and_model_settings.js');
var UserModel = models_and_model_settings['User'];

server.use(express.static('public'));

// Intitiate Mongoose
var mongoose = require('mongoose');
if (mongoose.connection.readyState == 0) {
    mongoose.connect('mongodb://localhost:27017/test');
    mongoose.Promise = require('bluebird');
}

// These middleware-functions makes it easier to fetch data and cookies from the requests that are sent 
server.use(body_parser.urlencoded({
    extended: true
}));
server.use(cookie_parser());


// This middleware-function fetches the user specified by the username that is sent with the cookie.
// If the user is properly authenticated the user-object is added to the request-object.
server.use(
    (req, res, next) => {
        if (req.headers) {

            var user_info_str = req.cookies.user_info;

            if (!user_info_str) {
                next();
            }
            else {

                var user_info = JSON.parse(user_info_str);
                var username = user_info.username;
                var login_cookie = user_info.login_cookie;

                UserModel.findOne({ username: username }).exec(

                    (error, user) => {

                        if (user) {

                            var matching_login_cookie = user.login_cookies.find(
                                cookie => cookie === login_cookie
                            );

                            if (matching_login_cookie) {
                                req.user = user;
                                next();
                            }
                            else {
                                console.log('Login cookie is not there');
                                next();
                            }

                        }
                        else {

                            console.log('User not found');
                            next();

                        }

                    }

                );

            }

        }
    }
)

server.get('/api', (req, res) => {

    res.json({
        message: 'Welcome to the glorious API of group 20 :)'
    });

});

// GET-endpoints
server.get('/api/find_one_entity', (req, res) => find_one_entity(req, res));
server.get('/api/retrieve', (req, res) => retrieve(req, res));

// POST-endpoints
server.post('/api/post_form', (req, res) => post_form(req, res));
server.post('/api/button_pushed', (req, res) => button_pushed(req, res));


if (IS_DEV) {

    // Starts up webpack dev-server

    var path = require('path');
    var config = require('../webpack.config_dev.js');
    var webpack = require('webpack');
    var webpackDevMiddleware = require('webpack-dev-middleware');
    var webpackHotMiddleware = require('webpack-hot-middleware');

    var compiler = webpack(config);

    server.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: config.output.publicPath }));
    server.use(webpackHotMiddleware(compiler));

    server.get('*', function (req, res) {
        res.sendFile(path.resolve('public/index_dev.html'));
    });

}
else {

    server.get('*', function (req, res) {
        res.sendFile(path.resolve('public/index_prod.html'));
    });

}


server.listen(PORT, (error) => {
    if (error) throw error;
    console.log("The server is listening on port", PORT);
});