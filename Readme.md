# Using the application

You can visit the application on [http://it2810-20.idi.ntnu.no/](http://it2810-20.idi.ntnu.no/).

This is the page you will be met with:

![alt tag](https://s21.postimg.org/hb3apa9s7/Screenshot_2016_11_16_04_24_31.png)


Register to be able to show your interest in buying or selling books:

![alt tag](https://s21.postimg.org/ndaxfry87/Screenshot_2016_11_16_04_27_20.png)


Thats better! The user system encrypts passwords before storing the database, and uses a login cookie for the current login *session* after the user has logged in or registered (until the user logs out or a month of time goes by and the cookie expires):

![alt tag](https://s21.postimg.org/61f682u4n/Screenshot_2016_11_16_04_27_56.png)


Click the load more button to see more of the books:

![alt tag](https://s21.postimg.org/cwzer6olh/Screenshot_2016_11_16_04_45_22.png)


Type in text in the search bar to filter books by whether their titles, or whole-word phrases cointaned within them, start with the string in question:

![alt tag](https://s21.postimg.org/j6uoe6o07/Screenshot_2016_11_16_04_29_39.png)


For each book you can click to see who is interesteded in buying or selling, and filter by whether they want to buy or sell (as well as a statistical overview with a diagram - if that doesn't qualify as "fancy" then what does?):

![alt tag](https://s21.postimg.org/5ia2yt2pz/Screenshot_2016_11_16_04_46_29.png)


Take a look at your own page with info about the books you want to buy or sell:

![alt tag](https://s21.postimg.org/r0v9zkvt3/Screenshot_2016_11_16_04_32_47.png)

The application does have the ability to do searches of books from the [Bokkildens API](http://www.bokkilden.no/SamboWeb/partneradmin.do?rom=MP) and convert these to the same format as the other books, so that they could be presented to the user based on searches, and/or stored to the database, but this capability has not been integrated into the application.

# Technologies
The main technologies we use are use are:

- [Express](http://expressjs.com/)
- [MongoDB/Mongoose](http://mongoosejs.com/)
- [React](https://facebook.github.io/react/)
- [React Router](https://www.npmjs.com/package/react-router)
- [Webpack](https://webpack.github.io/)
- [Redux](http://redux.js.org/)

We also make use of a variety of other libraries.

# Architectural choices
This architecture is not the easiest way to achieve such a small application as this, and whether it is a good architecture for large applications is also something that people would disagree upon. Some of the groups members may develop this code base further for use in developing complex websites, which is part of the reason for why the architecture is as it is.

This architecture prioritizes writing things in one place. If the same thing is done again and again with different tweaks then we have tried to have one function to take care of it, or to write helper-functions that can be reused every time that task is done, even if the work of doing so doesn't pay off in the short run. This philosophy could have been taken further than it has been though, and other projects building upon this code base may do so.

There are tradeoffs between architectures having a low learning curve and enabling the greatest efficiency for programmers who are comforable with it. This architecture leans towards the latter (although it of course is neither of the extremes, like most projects). This makes it harder to understand whats going on by giving it a brief glance though (although there surely also are things that could be done to the current code base to make it easier to understand that don't necessitate compromise in regards to this tradeoff).

Some of the architectural choices that have been made are:
 
### Containers
 
 The use of containers is standard when using React together with Redux. Containers are React-components that are connected to the Redux state using *react-redux*. Relevant parts of the state are then passed down from the container-components and down to the "normal" components using props. The containers are stored in */container_components*.
 
### Forms specified using form-objects
 
Here, for example, is the form-object that specifies how the form for registering a new user is to be presented to the user.
 
    {
        entity_type: 'User',
        form_type: 'Register',
        post_button_text: 'Register',
        fields: [
            {
                text: 'E-mail',
                value: '',
                field_type: 'textfield',
                pre_change_value: '',
                field_name: 'email'
            },
            {
                text: 'Username',
                value: '',
                field_type: 'textfield',
                pre_change_value: '',
                field_name: 'username'
            },
            {
                text: 'URL of profile image',
                value: '',
                field_type: 'textfield',
                pre_change_value: '',
                field_name: 'image'
            },
            {
                text: 'Password',
                value: '',
                field_type: 'textfield',
                pre_change_value: '',
                field_name: 'password',
                is_password_field: true
            }
        ]
    }
 
 Some about how form-objects are used:
 
- Form-objects are rendered using components/form/Form.jsx.
- Edits to a form (be that a text field being edited, an option being clicked, etc) result in Redux actions that modify the state of the form in question).
- Forms that are posted to the backend are processed by queries/query_receivers/post_form.js.
- There are also functions that modify forms (change the title with a given field_name, remove the field with a given field_name, etc). E.g. the *Buy book*-form and the *Sell book*-form are based on the same form-object.
- Buttons that are clicked can also send in form-objects that have been modified by the front-end (but not explicitly by the user). This makes it so that less API-calls have to be supported by the backend. This is however not what is done for every button (admins, for example, have a button that lets them delete books, and this is not done using form-objects)

At the scale of this application this structure is a cumbersome way of doing things, but it is hoped that more complex applications with many forms and more complex form-related functionality may benefit from this architecture if they are built upon a further evolved version of this code base.

### Queries represented by query-objects

Here is an example of a query object:

    {
        query_type: 'retrieve',
        entity_type: 'Book',
        find_several_entities: true,
        parameters: {
            user_ref: '54759eb3c090d83494e2d804'
        },
        limit: elements_per_page,
        skip: 18,
        key_for_storage_of_result: `book_page_3`
    }

Some about how query-objects are used:

- The query_type of a query-object corresponds to the function on the backend that receives it. The query in the example above is handled by *queries/query_receivers/retrieve.js*.
- Info about query-objects are stored in the Redux state. To find the result of a query that has been sent and gotten a response we may hash the query-object and see if there is a result in the Redux state that is stored by that hash. The same is true if we want to see if a query already has been sent and when. This is handled in *redux/reducers/reducer_data.js*.
- There are functions that for a given URL returns the queries that should be sent. The container can then easily check if the queries that should be sent for it already have results stored in in the Redux state, and a function can decide based on the Redux state which of the queries that should be sent to the server (if none of them have been sent previously then all of them should, but other cases may be more nuanced). Queries can also correspond to fields (in form-objects or outside of form-objects) instead of URLs, which could be useful when implementing e.g. autocomplete, but this has not been made use of in this project. All this is taken care of in *queries/get_queries.js*.

It is likely that 1-2 of us will keep using this approach in later web projects, though not without some significant refactoring.

### Inline stylings

[Inline stylings](https://www.youtube.com/watch?v=0ZNIQOO2sfA) have been used with help from the library [Aprhodite](https://www.npmjs.com/package/aphrodite), as well as self-written helper-functions.

Below is an example of how the button that posts forms is implemented:

    render() {
        var style = {
            userSelect: 'none',
            paddingLeft: '15px',
            paddingRight: '15px',
            paddingTop: '8px',
            paddingBottom: '8px',
            borderRadius: '2px',
            backgroundColor: s('backgroundColor', 'ActionButton'),
            color: 'white',
            fontSize: '15px',
            display: 'flex',
            justifyContent: 'center',
            fontWeight: '500',
            ':hover': {
                cursor: 'pointer',
                backgroundColor: s('backgroundColor', 'FlatButton', ':hover')
            }
        };

        return (
            <Paper className={css(style)} onClick={this.props.onClick}>
                {this.props.children}
            </Paper>
        );

    }

The *s('backgroundColor', 'ActionButton')* fetches variables from *styling/style_vars.js*, where some of the style variables are stored in a common location.

The css-function in *className={css(style)}* is implemented in helpers/style_helpers. It makes use of [Aprhodite](https://www.npmjs.com/package/aphrodite) to do the great majority of the work, but helps do merging of styles (including what is nested in pseudo-classes like *:hover* and so on) if there are more of them passed in at once, and makes the syntax a bit more convenient.

It can be said in favour of inlone styling that it easier to make components as one has to switch between less files. While seperation of conecrns is a good thing it may make more sense to seperate by component than whether it is JavaScript or CSS or HTML. People will have different opinions about this though.

# Folder structure

Here is a description of the structure of the project. Only the more important files and folders are mentioned. The nesting of the headlines corresponds to the nestings of folders.

### client_setup/
Where the client is initialized.

##### client_setup/client.js

Initizes the client (sets up React Router, etc). Everything else on the frontend is imported from here.

### components/

React components of variour types.

##### components/form/

React components that implement forms (Form.jsx receives a settings-object and renders the form based in this).

##### container_components/

Components that helps connect React and Redux. React components are given props based on the redux state.

### container_components/BookListContainer.jsx

Used to present the view where books are listed.

##### container_components/TransferralRequestListContainer.jsx

Used to present the view where the people who are interested in buying or selling a given book are listed.

##### container_components/MainContainer.jsx

Everything is wrapped inside of this container. Used to decide what the navbar should look like, if forms should be displayed, etc, based on the redux state.

### form_types/

Settings-object for different forms. One setting-object can be used for several different forms by having it be mofified by a function.

### helpers/

Functions that help with varios tasks. If there is no obvious place for a piece of code in the project it will often be placed here. 

### models/

Specification of Mongoose models.

##### models/modedels_and_model_settings.js

This file contains all of the mongoose models, as well as other stuff, such as settings for these models (though these settings are not currently made use of).

### public/

This is where the index.html-files (one for the dev environment and one for the prod environment) are stored.

### queries/

This folder stores files that make queries or receive queries.

##### queries/query_receivers/post_form.js

This is the endpoint where all forms are processed, but it has other functions that it delegates the forms to, based on what kind of form it is.

##### queries/query_receivers/button_pushed.js

Used to receive buttons that are being pushed. Currenly it is only used for the delete-button (which is available for admins), as the other buttons are implemented by having a form-settings-object be sent and processed like other forms.

##### queries/query_receivers/retrieve.js

Any query that searches for one entity, several entities, or wants to count how many entities fulfill some criteria, are sent to this endpoint.

### query_senders/
Functions that send queries to the backend.

##### queries/query_senders/do_get_query.js
This function is used to make all get-queries, and to update the redux state based on the queries that are sent and received.

##### queries/query_senders/do_query_for_container_or_field.js
This function takes in a reference to a container or URL or form field, and does the queries for it (unless they already have been retrieved recently according to the redux state).

##### queries/query_senders/post_form.js
Used when user-filled forms are sent to the backend.


### redux/
This is where redux-related things are stored.

##### redux/reducers/
The redux-reducers themselves are contained here.

##### redux/reducer_functions/
For every action that can be reduced there is a function that the reducer makes use of to handle it, and these functions are stored here.

##### redux/state_getters/
Helper-functions for reducer-functions that fetches stuff from the redux state. Also used from elsewhere in the application.

##### redux/state_replacers/
Helper-functions for reducer-functions that update the state.

##### action_creators.js
This is where all redux actions are generated. Some of the functions here also do things outside of redux.

##### redux_store.js
This is where the setup for ther redux store is.

### scripts/
Here we put functions that are initiated from the command line (and potentially, though not in this project, could be run automatically e.g. every hour or every day).

##### scripts/add_initial_data.js
Adds data to the application so that there is something to start with. Not currently in use.

##### scripts/add_books_from_bokkilden.js
Fetches some books from bokkildens API.

##### scripts/delete_all_data.js
Deletes all the data  in the application. Useful when in development mode.

##### scripts/server_setup/
This is where the server is set up.

### styling/
Files that are related to style settings. Currently it only contains the file style_vars, but this file could potentially be split into several if it grows sufficiently large.

##### styling/style_vars.js
This is where styling variables that are used in the application are stored.

### webpack.config.js
The webpack config for production mode.

### webpack.config_dev.js
The webpack config for development mode.

### webpack_config_resolve.js
This file makes it so that we can import import files the name only (e.g. *import ListOfObjectsWrapper from 'ListOfObjectsWrapper'*), instead of having to specify the path everywhere that it's made use of (e.g. *import ListOfObjectsWrapper from '../.../wrappers/ListOfObjectsWrapper'*).