import style_helpers from 'style_helpers';
import darken_color_by_percentage from 'darken_color_by_percentage';
import lighten_color_by_percentage from 'lighten_color_by_percentage';

var Obj = {};

// HEIGHT:

var height = {
    TextField: '27px',
    Navbar: '56px',
    BookBox: '70px',
    _BookBox: {
        image: '130px',
        div_below_image: '70px'
    }
};
Obj.height = height;


// WIDTH:

var width = {
    TextField: '500px',
    BookListContainer: '920px',
    NonPopupFormWrapper: '920px'
};
Obj.width = width;


// MARGIN-TOP:

var marginTop = {
    BookListContainer: '35px',
    NonPopupFormWrapper: '35px',
    BookBox: {
        title: '6px'
    }
};
Obj.marginTop = marginTop;


// PADDING:

var padding = {};
Obj.padding = padding;

var paddingLeftAndRight = {
    TextField: '5px',
    BookBox: '5%',
    Navbar: {
        navbar_alternatives: '8px'
    }
};
Obj.paddingLeftAndRight = paddingLeftAndRight;

var paddingTopAndBottom = {
    Option: '4px',
    BookBox: {
        div_below_image: '5px'
    }
};
Obj.paddingTopAndBottom = paddingTopAndBottom;

var paddingLeft = {
    Option: '5px'
};
Obj.paddingLeft = paddingLeft;

var paddingRight = {};
Obj.paddingRight = paddingRight;

var paddingTop = {
    BookBox: {
        div_with_buttons: '8px'
    }
};
Obj.paddingTop = paddingTop;

var paddingBottom = {};
Obj.paddingBottom = paddingBottom;


// MARGIN-RIGHT:

var marginLeft = {
    Navbar: {
        ':nth-of-type(1)': '20px'
    },
    BookBox: {
        flat_button: {
            ':nth-child(2)': '4px'
        }
    }
};
Obj.marginLeft = marginLeft;





// BACKGROUND-COLOR

var backgroundColor = {
    // GLOBAL: '#F1F1F1',
    GLOBAL: 'rgba(0, 0, 0, 0.02)',
    // TextField: 'rgba(0, 0, 0, 0.02)',
    ActionButton: darken_color_by_percentage('#0288D1', 2),
    _ActionButton: {
        ':hover': darken_color_by_percentage('#0288D1', 10)
    },
    SearchBar: lighten_color_by_percentage('#0288D1', 30),
    _SearchBar: {
        ':focus': 'white'
    },
    TextField: 'white',
    _TextField: {
        ':focus': 'white'
    },
    Option: 'rgba(0, 0, 0, 0.02)',
    _Option: {
        ':hover': 'rgba(0, 0, 0, 0.1)',
        selected: 'rgba(0, 0, 0, 0.20)',
        _selected: {
            ':hover': 'rgba(0, 0, 0, 0.20)'
        }
    },
    Navbar: darken_color_by_percentage('#0288D1', 7),
    _Navbar: {
        navbar_alternatives: {
            ':hover': darken_color_by_percentage('#0288D1', 14)
        }
    },
    BookBox: {
        div_below_image: 'white',
        hover_div: 'rgba(150, 200, 255, 0.08)' //#FAFAFA
    },
};
Obj.backgroundColor = backgroundColor;


// COLOR

var color = {
    Option: {
        style_text: 'rgba(0, 0, 0, 0.7)',
        style_selected_text: 'rgba(0, 0, 0, 0.8)'
    },
    Navbar: {
        navbar_alternatives: 'white'
    },
    BookBox: {
        title: 'rgba(0, 0, 0, 0.8)'
    }
};
Obj.color = color;


// FONT-FAMILY:

var fontFamily = {
    GLOBAL: 'Helvetica, sans-serif'
};
Obj.fontFamily = fontFamily;


// FONT-SIZE:

var fontSize = {
    TextField: '15px',
    BookBox: {
        title: '18px'
    }
};
Obj.fontSize = fontSize;


// BORDER_RADIUS:

var borderRadius = {
    NonPopupFormWrapper: '2px',
    Option: '2px',
    TextField: '2px',
    BookBox: '2px'
};
Obj.borderRadius = borderRadius;


// BORDER

var border = {
    Option: '1px solid rgba(0, 0, 0, 0.1)',
    TextField: '1px solid #E9E9E9',
    _TextField: {
        ':focus': `1px solid #4285f4`
    }
};
Obj.border = border;


module.exports = (property, component_referant_or_other_top_level_referant, style_obj_referant, selector_referant) => {

    return style_helpers.fetch_style_vars(Obj, property, component_referant_or_other_top_level_referant, style_obj_referant, selector_referant);

};