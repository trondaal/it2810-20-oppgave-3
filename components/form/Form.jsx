// Libraries:
import React from 'react';
import hash_it from 'hash_it';

// Components
import FormField from 'FormField';
import ActionButton from 'ActionButton';

import post_form from 'post_form';


module.exports = React.createClass({


    render_form_intro() {

        // TODO

        var obj = this.props.obj;
        var {text} = obj;

        if (!text) return null;

        var s_text = {
            fontSize: '30px',
            color: 'rgba(0, 0, 0, 0.75)',
            fontWeight: 'bold',
            marginBottom: '25px'
        }

        return (
            <div>
                <div style={s_text}>{text}</div>
            </div>
        );

    },

    render_form_outro() {

        // TODO

        return <div></div>;

    },

    render_post_button() {

        var post_button_text = this.props.obj.post_button_text;
        if (!post_button_text) post_button_text = 'Post';

        var style_post_form_button_div = {
            display: 'flex'
        };

        return (
            <div style={style_post_form_button_div}>
                <ActionButton onClick={this.post_form}>{post_button_text}</ActionButton>
            </div>
        );

    },

    post_form() {

        var {state, obj, location} = this.props;

        if (!state) throw new Error();
        if (!obj) throw new Error();

        post_form(state, obj, location);

    },

    render_fields(obj) {

        var obj = this.props.obj;

        var {form_key, fields, style_type} = obj;

        if (!style_type) style_type = 'classical';

        var fields_to_display = fields.filter(
            (field) => {
                return !field.dont_display;
            }
        );

        return fields_to_display.map(
            (field, index, arr) =>
                (
                    <FormField
                        is_bottom_field={index === arr.length - 1}
                        is_top_field={index === 0}
                        style_type={style_type} form_key={form_key}
                        key={hash_it(field.field_key)}
                        obj={field}
                        state={this.props.state} />
                )
        );

    },

    render() {

        var obj = this.props.obj;

        if (!obj) throw Error();

        var style_form_div = {
            padding: '20px'
        };

        var rendered_form_intro = this.render_form_intro();
        var rendered_fields = this.render_fields();
        var rendered_form_outro = this.render_form_outro();
        var rendered_post_button = this.render_post_button();

        return (
            <div style={style_form_div}>
                {rendered_form_intro}
                {rendered_fields}
                {rendered_form_outro}
                {rendered_post_button}
            </div>
        );

    }

});