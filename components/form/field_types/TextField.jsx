// Libraries
import React from 'react';

import action_creators from 'action_creators';

import s from 'style_vars';

import css from 'css';

module.exports = React.createClass({

    onClick(event) {
        
    },

    onKeyDown(event) {

        if (event.key === 'ArrowDown') {
            //TODO
        }

        else if (event.key === 'ArrowUp') {
            //TODO
        }

        else if (event.key === 'Enter') {
            //TODO
        }

    },

    onChange(event) {

        var form_key = this.props.form_key;

        var new_value = event.target.value;

        if (new_value === null || new_value === undefined) value = '';

        var old_field = this.props.obj;
        var new_field = Object.assign({}, old_field);
        new_field.value = new_value;

        action_creators.REPLACE_FIELD(old_field, new_field, form_key);

    },

    render() {

        var obj = this.props.obj;
        var value = obj.value;

        var style = {
            width: s('width', 'TextField'),
            height: s('height', 'TextField'),
            paddingLeft: s('paddingLeftAndRight', 'TextField'),
            paddingRight: s('paddingLeftAndRight', 'TextField'),
            fontSize: s('fontSize', 'TextField'),
            border: s('border', 'TextField'),
            borderRadius: s('borderRadius', 'TextField'),
            backgroundColor: s('backgroundColor', 'TextField'),
            ':focus': {
                backgroundColor: s('backgroundColor', 'TextField', ':focus'),
                border: s('border', 'TextField', ':focus'),
                outline: 'none'
            }
        };

        var text_field_type = obj.is_password_field ? 'password' : 'text';

        return (
            <input
                type={text_field_type}
                className={css(style)}
                value={value}
                onChange={this.onChange}
                onClick={this.onClick}
                onKeyDown={this.onKeyDown} />
        );

    }

});
