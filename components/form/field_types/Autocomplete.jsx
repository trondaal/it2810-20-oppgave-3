// Libraries
import React from 'react';
import hash_it from 'hash_it';

// Components
import Paper from 'Paper';

import action_creators from 'action_creators';

import s from 'style_vars';

import css from 'css';


var change_index_of_list = (autocomplete_obj) => {

    

};

var fetch_dropdown_options = (autocomplete_obj) => {

    if (!autocomplete_obj.initial_dropdown_options) return [];

    if (autocomplete_obj.value) {

        return autocomplete_obj.initial_dropdown_options.filter(
            (dropdown_option) => {
                
            }
        );

    }

    return autocomplete_obj.initial_dropdown_options;    

};

var DropdownOption = React.createClass({

    render() {

        var obj = this.props.obj;

        var style = {
            display: 'flex',
            alignItems: 'center',
            backgroundColor: 'white',
            height: s('height', 'TextField'),
            fontSize: s('fontSize', 'TextField'),
            paddingLeft: s('paddingLeftAndRight', 'TextField'),
            paddingRight: s('paddingLeftAndRight', 'TextField'),
            ':hover': {
                cursor: 'pointer',
                backgroundColor: 'rgb(235, 238, 244)'
            }
        };

        var s_text = {

        };

        return (
            <div className={css(style)}>
                {obj.text}
            </div>
        );

    }

});

var Dropdown = React.createClass({

    render() {

        var obj = this.props.obj;

        var style = {
            position: 'absolute',
            width: s('width', 'TextField', 'return number') + s('paddingLeftAndRight', 'TextField', 'return number') * 2 + 'px',
            borderLeft: s('border', 'TextField'),
            borderRight: s('border', 'TextField'),
            borderBottom: s('border', 'TextField'),
            borderBottomLeftRadius: s('borderRadius', 'TextField'),
            borderBottomRightRadius: s('borderRadius', 'TextField')
        };

        var dropdown_options = fetch_dropdown_options(obj);

        var rendered_dropdow_options = dropdown_options.map(
            (dropdown_option) => <DropdownOption
                obj={dropdown_option}
                key={hash_it(dropdown_option)} />
        );

        return (
            <div style={style}>
                {rendered_dropdow_options}
            </div>
        )

    }

});


module.exports = React.createClass({

    onClick(event) {

    },

    onKeyDown(event) {

        if (event.key === 'ArrowDown') {
        }

        else if (event.key === 'ArrowUp') {
        }

        else if (event.key === 'Enter') {
        }

    },

    onChange(event) {

        var form_key = this.props.form_key;

        var new_value = event.target.value;

        if (new_value === null || new_value === undefined) value = '';

        var old_field = this.props.obj;
        var new_field = Object.assign({}, old_field);
        new_field.value = new_value;

        action_creators.REPLACE_FIELD(old_field, new_field, form_key);

    },

    render() {

        var obj = this.props.obj;
        var value = obj.value;

        var style = {
            width: s('width', 'TextField'),
            height: s('height', 'TextField'),
            paddingLeft: s('paddingLeftAndRight', 'TextField'),
            paddingRight: s('paddingLeftAndRight', 'TextField'),
            fontSize: s('fontSize', 'TextField'),
            border: s('border', 'TextField'),
            borderRadius: s('borderRadius', 'TextField'),
            backgroundColor: s('backgroundColor', 'TextField'),
            ':focus': {
                backgroundColor: s('backgroundColor', 'TextField', ':focus'),
                border: s('border', 'TextField', ':focus'),
                outline: 'none'
            }
        };

        var text_field_type = obj.is_password_field ? 'password' : 'text';

        return (
            <div>
                <input
                    type={text_field_type}
                    className={css(style)}
                    value={value}
                    onChange={this.onChange}
                    onClick={this.onClick}
                    onKeyDown={this.onKeyDown} />
                <Dropdown obj={obj} />
            </div>
        );

    }

});
