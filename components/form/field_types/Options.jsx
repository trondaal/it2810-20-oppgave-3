// Libraries
import React from 'react';
import hash_it from 'hash_it';

// Helpers
import process_user_input_based_html_for_react from 'process_user_input_based_html_for_react';
import css from 'css';

// Components
import ListOfObjectsWrapper from 'ListOfObjectsWrapper';
import Paper from 'Paper';

import action_creators from 'action_creators';

import s from 'style_vars';

var Option = React.createClass({

    on_click() {

        var form_key = this.props.form_key;
        var option_key = this.props.obj.option_key;

        var old_field = this.props.obj_of_field;
        var new_field = Object.assign({}, old_field);
        
        var max_number_of_selections_allowed = new_field.max_number_of_selections_allowed;
        var number_of_selections = new_field.options.filter(option => option.is_selected).length;

        if (max_number_of_selections_allowed === 1) {

            for (var option of new_field.options) {

                if (option.option_key === option_key) {
                    if (option.is_selected) option.is_selected = false;
                    else option.is_selected = true;
                }
                else {
                    option.is_selected = false;
                }

            }

        }
        else if (number_of_selections < max_number_of_selections_allowed) {

            for (var option of new_field.options) {

                if (option.option_key === option_key) {
                    if (option.is_selected) option.is_selected = false;
                    else option.is_selected = true;
                }

            }

        }
        else {

            for (var option of new_field.options) {

                if (option.option_key === option_key) {
                    if (option.is_selected) option.is_selected = false;
                    else option.error_message = `A maximum of ${max_number_of_selections_allowed} alternatives can be chosen here.`;
                }

            }

        }

        if (max_number_of_selections_allowed === 1) {
            var selected_option = new_field.options.find(option => option.is_selected);
            if (selected_option) new_field.value = selected_option.value;
            else new_field.value = null;
        }
        else {
            new_field.value = new_field.options.filter(option => option.is_selected).map(option => option.value);
        }

        action_creators.REPLACE_FIELD(old_field, new_field, form_key);

    },

    generate_signifier(index, number_of_options) {

        if (number_of_options > 1 && number_of_options <= 26) {
            var alphabet_arr = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
            return alphabet_arr[number_of_options - 1];
        }

        return null;

    },

    render_box() {

        var style_given_as_prop = this.props.style;

        throw new Error('Functionality for rendering boxes is not yet written.');

    },

    render_line() {

        var obj = this.props.obj;

        var text = obj.text;
        var extra_text = obj.extra_text;
        var extra_extra_text = obj.extra_extra_text;
        var option_style = this.props.option_style;
        var index = this.props.index;
        var number_of_options = this.props.number_of_options;
        var has_input_field = this.props.has_input_field;
        var key = this.props.option_key;
        var is_selected = obj.is_selected;

        var extra_text_div_rendered;

        if (extra_text) {
            extra_text_div_rendered = <div dangerouslySetInnerHTML={process_user_input_based_html_for_react(extra_text)} />;
        }

        var style = {
            width: '100%',
            userSelect: 'none',
            borderRadius: s('borderRadius', 'Option'),
            paddingLeft: s('paddingLeft', 'Option'),
            paddingTop: s('paddingTopAndBottom', 'Option'),
            paddingBottom: s('paddingTopAndBottom', 'Option'),
            border: s('border', 'Option'),
            backgroundColor: s('backgroundColor', 'Option'),
            ':hover': {
                cursor: 'pointer',
                backgroundColor: s('backgroundColor', 'Option', ':hover')
            }
        }

        var selected = {
            backgroundColor: s('backgroundColor', 'Option', 'selected'),
            ':hover': {
                backgroundColor: s('backgroundColor', 'Option', 'selected', ':hover')
            }
        };

        var style_text = {
            flexGrow: '1',
            color: s('color', 'Option', 'style_text')
        };

        var style_selected_text = {
            flexGrow: '1',
            color: s('color', 'Option', 'style_selected_text')
        };

        if (is_selected) {

            return (
                <div className={css([style, selected])} onClick={this.on_click} key={key}>
                    <div style={style_selected_text} dangerouslySetInnerHTML={process_user_input_based_html_for_react(text)} />
                </div>
            );

        }
        else {

            return (
                <div className={css(style)} onClick={this.on_click} key={key}>
                    <div style={style_text} dangerouslySetInnerHTML={process_user_input_based_html_for_react(text)} />
                </div>
            );

        }



    },

    render() {

        var has_input_field = this.props.has_input_field;

        if (has_input_field) throw Error('The feature of options with input fields is not yet supported');

        if (this.props.render_boxes_and_not_lines) {

            throw Error('Functionality for rendering boxes is not yet written.');

        } else {

            return this.render_line();

        }

    }

});

module.exports = React.createClass({

    is_to_render_boxes_and_not_lines() {

        //TODO: Write this function

        return false;

    },

    render() {

        var obj = this.props.obj;

        var number_of_options = obj.options.length;

        var render_boxes_and_not_lines = this.is_to_render_boxes_and_not_lines();

        var options = obj.options;
        var keys = options.map(option => hash_it(option));

        var specific_style_container = null;
        var specific_style_options = null;

        var rendered_options_for_wrapper = options.map(
            (option, index) => {
                return (
                    <Option obj={option}
                        obj_of_field={obj}
                        field_key={obj.field_key}
                        key={option.option_key}
                        dispatch={this.props.dispatch}
                        index={index}
                        render_boxes_and_not_lines={render_boxes_and_not_lines}
                        number_of_options={number_of_options}
                        form_key={this.props.form_key} />
                );
            }
        );

        if (render_boxes_and_not_lines) {

            throw new Error('This functionality is not yet supported.');

        }
        else {

            return (
                <ListOfObjectsWrapper
                    objects={options}
                    rendered_objects={rendered_options_for_wrapper}
                    component_called_from={'Options'} />
            );

        }

    }

});