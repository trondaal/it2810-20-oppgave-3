// Libraries
import React from 'react';

// Components
import Options from 'Options';
import TextField from 'TextField';
import Autocomplete from 'Autocomplete';

import get_data from 'get_data';

import do_queries_for_container_or_field from 'do_queries_for_container_or_field';

module.exports = React.createClass({

    render_field_header() {

        var {obj, style_type} = this.props;
        var {text, extra_text, extra_extra_text} = obj;

        if (style_type === 'classical') {

            var style = {
                paddingBottom: '5px',
                paddingLeft: '1px'
            };

            var style_text = {
                fontWeight: '800',
                fontSize: '19px',
                color: 'rgba(0, 0, 0, 0.75)'
            };

            var style_extra_text = {
                marginTop: '5px',
                color: 'rgba(0,0,0,0.75)',
                cornerRadius: '3px',
                fontSize: '15px'
            };

            return (
                <div style={style}>
                    <div style={style_text} dangerouslySetInnerHTML={{ __html: text }}></div>
                    <div style={style_extra_text} dangerouslySetInnerHTML={{ __html: extra_text }}></div>
                </div>
            );

        } else {

            throw Error('The style_type of all forms must be one of the specified ones.');

        }

    },

    render_query_result() {

        // TODO, remove
        return null;

        var {obj, style_type, state} = this.props;

        var queries = obj.queries;

        if (!obj.queries) return null;

        var data = get_data.for_field(state, obj);

        if (!data.books) return null;

        return (
            <div>
                {
                    data.books.map(
                        book => <div>{book.title}</div>
                    )
                }
            </div>
        );

    },

    render_main_component() {

        var style = {

        };

        var {obj, form_key, style_type} = this.props;
        var field_type = obj.field_type;

        switch (field_type) {
            case 'textfield':
                return <TextField style_type={style_type} form_key={form_key} obj={obj} />;
            case 'options':
                return <Options style_type={style_type} form_key={form_key} obj={obj} />;
            case 'autocomplete':
                return <Autocomplete style_type={style_type} form_key={form_key} obj={obj} />;
            default:
                throw new Error('We do not suppert this field_type')
        }

    },

    render() {

        var style = {
            paddingBottom: '22px'
        };

        var field_header_rendered = this.render_field_header();
        var main_component_rendered = this.render_main_component();
        var query_result_rendered = this.render_query_result();

        return (
            <div style={style}>
                {field_header_rendered}
                {main_component_rendered}
                {query_result_rendered}
            </div>
        );

    },

    componentDidMount() {

        var {obj, state} = this.props;

        if (!obj.queries) return;

        do_queries_for_container_or_field(state, null, null, obj);

    }

});