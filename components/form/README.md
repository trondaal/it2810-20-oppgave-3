# Forms

### Summary of architecture
Forms are represented as JS objects. These objects contain info about the state of the form, the info in the form, how the form is to look, how the data is to be persisted in the database, etc. Ideally, the task of making a new form should consist of only specifying a new JS object containing the forms specification.

So when people do stuff on the form the form-object is changed (via Redux actions), and based on these changes the form is displayed differently (since React renders the form based on what is specified in the form-object). And when it is posted to the database this is done using a function and one function only, that is used to process all form-postings and does so with help from the specifications given in the form-object (which tells it where in the database different fields belong, etc).

### Where to find forms-related code
The other main form-related pars of the code can be found in the following places:

- components/form
    - the React components that forms are made from, minus the general-purpose components that also are used elsewhere
- redux_reducers/forms_and_fields_reducer.js
    - updates the state when a form is updated, etc
- helpers/form_and_autocomplete_helpers
    - does thing regarding the state having to do with form, like e.g. fetching the form from the state with a specific form_key

### Types of form fields

##### TextField

##### Options

##### AutoComplete

##### Options

### Properties of form-objects

##### *fields*

### Properties of field-objects

##### *text*, *extra_text* and *extra_extra_text*

##### *field_type*

##### *secondary_field_type*

##### *fieldname*

##### *field_key*

##### *display_if*

##### *fields*

##### *value*

##### *pre_change_value*

##### *min_number_of_selections_allowed* and *max_number_of_selections_allowed*
