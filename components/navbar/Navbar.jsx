// Libraries:
import React from 'react';
import { Link } from 'react-router';

import SearchIcon from 'react-icons/lib/md/search';

// Components
import Paper from 'Paper';

import action_creators from 'action_creators';
import do_queries_for_container_or_field from 'do_queries_for_container_or_field';
import global_vars_public from 'global_vars_public';
import url_properties_from_location_or_url_str from 'url_properties_from_location_or_url_str';
import css from 'css';
import s from 'style_vars';


module.exports = React.createClass({

    on_search_bar_change(event) {

        var form_key = this.props.form_key;

        var new_value = event.target.value;

        var new_state = Object.assign(
            {},
            this.props.state,
            {
                forms_and_fields: {
                    search_bar_value: new_value
                }
            }
        );

        do_queries_for_container_or_field(
            new_state,
            false,
            url_properties_from_location_or_url_str(this.props.location),
            null
        );

        action_creators.NEW_SEARCH_BAR_VALUE(new_value);

    },

    render_search_bar() {

        var {state, location} = this.props;

        var style = {
            height: '36px',
            marginTop: '12px',
            display: 'flex',
            flexGrow: '1'
        };

        var s_input = {
            height: '30px',
            paddingLeft: '8px',
            paddingRight: s('paddingLeftAndRight', 'TextField'),
            fontSize: s('fontSize', 'TextField'),
            border: 'none',
            borderTopLeftRadius: s('borderRadius', 'TextField'),
            borderBottomLeftRadius: s('borderRadius', 'TextField'),
            width: '55%',
            marginLeft: '16%',
            color: 'rgba(0, 0, 0, 0.85)',
            backgroundColor: 'rgb(245, 245, 245)',
            ':focus': {
                border: 'none',
                outline: 'none'
            }
        };

        var s_search_icon_container = {
            backgroundColor: 'white',
            borderTopRightRadius: s('borderRadius', 'TextField'),
            borderBottomRightRadius: s('borderRadius', 'TextField'),
            height: '32px',
            paddingBottom: '0px'
        };

        var s_search_icon = {
            color: 'grey',
            paddingTop: '2px',
            paddingRight: '4px',
            paddingLeft: '6px',
            transform: 'rotate(90deg)'
        };

        return (
            <div className={css(style)}>
                <input
                    className={css(s_input)}
                    value={state.forms_and_fields.search_bar_value}
                    onChange={this.on_search_bar_change} />
                <Paper style={s_search_icon_container}>
                    <SearchIcon size={28} style={s_search_icon} />
                </Paper>
            </div>
        );

    },

    change_key_of_current_dialog: key => () => {
        action_creators.CHANGE_KEY_OF_CURRENT_DIALOG(key, null);
    },

    link_to(to_value) {

        if (to_value === '/my_books') {

            var {state, location} = this.props;

            if (!state) throw new Error();
            if (!location) throw new Error();

            return () => {
                do_queries_for_container_or_field(
                    state,
                    false,
                    url_properties_from_location_or_url_str('/my_books'),
                    null
                );
                action_creators.PUSH(to_value);
            }

        }
        else {
            return () => action_creators.PUSH(to_value);
        }



    },

    log_out() {

        action_creators.UPDATE_TO_LOGGED_OUT();

    },

    render() {

        var {state, location} = this.props;

        var style = {
            backgroundColor: s('backgroundColor', 'Navbar'),
            height: s('height', 'Navbar'),
            display: 'flex',
            position: 'fixed',
            width: '100%',
            top: '0px',
            zIndex: 10
        };

        var navbar_alternatives_block = {
            display: 'flex',
            height: s('height', 'Navbar'),
            justifyContent: 'flex-end',

        };

        var navbar_alternatives = {
            height: s('height', 'Navbar'),
            color: s('color', 'Navbar', 'navbar_alternatives'),
            alignItems: 'center',
            display: 'flex',
            paddingLeft: s('paddingLeftAndRight', 'Navbar', 'navbar_alternatives'),
            paddingRight: s('paddingLeftAndRight', 'Navbar', 'navbar_alternatives'),
            textDecoration: 'none',
            userSelect: 'none',
            cursor: 'pointer',
            ':hover': {
                backgroundColor: s('backgroundColor', 'Navbar', 'navbar_alternatives', ':hover')
            }
        };

        var search_bar = this.render_search_bar();

        if (state.data.user_info) {

            var user_info = state.data.user_info;

            var image = user_info.image;
            if (!image) image = global_vars_public.default_profile_image;

            var s_rounded_profile_image = {
                borderRadius: '50%',
                backgroundImage: `url(${image})`,
                backgroundSize: 'cover',
                backgroundPosition: '50% 50%',
                marginLeft: '7px',
                width: '20px',
                height: '20px'
            };

            var rounded_profile_image = (
                <div style={s_rounded_profile_image} />
            );

            return (
                <Paper className={css(style)}>
                    <div className={css(navbar_alternatives_block)}>
                        <Link to='/' className={css([navbar_alternatives, { marginLeft: '10px' }])} >Home</Link>
                        <div onClick={this.change_key_of_current_dialog('Book_buy_book')} className={css(navbar_alternatives)}>Buy book</div>
                        <div onClick={this.change_key_of_current_dialog('Book_sell_book')} className={css(navbar_alternatives)}>Sell book</div>
                    </div>
                    {search_bar}
                    <div className={css(navbar_alternatives_block)}>
                        <div onClick={this.link_to('/my_books')} className={css(navbar_alternatives)}>
                            {user_info.username}
                            {rounded_profile_image}
                        </div>
                        <div className={css([navbar_alternatives, { marginRight: '10px', textDecoration: 'none' }])} onClick={this.log_out}>Log out</div>
                    </div>
                </Paper>
            );

        }
        else {

            return (
                <Paper className={css(style)}>
                    <div className={css(navbar_alternatives_block)}>
                        <Link to='/' className={css([navbar_alternatives, { marginLeft: '10px' }])} >Home</Link>
                    </div>
                    {search_bar}
                    <div className={css(navbar_alternatives_block)}>
                        <div className={css(navbar_alternatives)} onClick={this.change_key_of_current_dialog('LogIn')}>Log in</div>
                        <div className={css([navbar_alternatives, { marginRight: '10px' }])} onClick={this.change_key_of_current_dialog('Register')}>Register</div>
                    </div>
                </Paper>
            );

        }

    }

});