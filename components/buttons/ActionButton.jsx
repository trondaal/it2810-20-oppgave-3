// Libraries
import React from 'react';

// Components
import Paper from 'Paper';

import css from 'css';
import s from 'style_vars';

module.exports = React.createClass({

    render() {

        var style = {
            userSelect: 'none',
            paddingLeft: '15px',
            paddingRight: '15px',
            paddingTop: '8px',
            paddingBottom: '8px',
            borderRadius: '2px',
            backgroundColor: s('backgroundColor', 'ActionButton'),
            color: 'white',
            fontSize: '15px',
            display: 'flex',
            justifyContent: 'center',
            fontWeight: '500',
            ':hover': {
                cursor: 'pointer',
                backgroundColor: s('backgroundColor', 'ActionButton', ':hover')
            }
        };

        return (
            <Paper className={css(style)} onClick={this.props.onClick}>
                {this.props.children}
            </Paper>
        );

    }

});