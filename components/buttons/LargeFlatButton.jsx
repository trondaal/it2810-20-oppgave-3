// Libraries
import React from 'react';

// Components
import Paper from 'Paper';

import css from 'css';

module.exports = React.createClass({

    render() {

        var style = {
            borderRadius: '2px',
            fontSize: '17px',
            display: 'flex',
            marginTop: '20px',
            marginLeft: '45%',
            marginRight: '45%',
            paddingTop: '7px',
            paddingBottom: '7px',
            paddingLeft: '10px',
            paddingRight: '10px',
            justifyContent: 'center',
            fontWeight: '500',
            color: 'rgba(0, 0, 0, 0.7)',
            border: '1px solid rgba(0, 0, 0, 0.1)',
            zIndex: 5,
            backgroundColor: 'rgb(250, 250, 250)',
            ':hover': {
                cursor: 'pointer',
                backgroundColor: 'rgb(245, 245, 245)'
            }
        };

        if (typeof this.props.className === 'string') {
            throw new Error('Pass styles to FlatButton as objects, not already from elsewhere.')
        }

        return (
            <div className={css([style, this.props.className, this.props.style])} onClick={this.props.onClick} onMouseEnter={this.props.onMouseEnter} onMouseLeave={this.props.onMouseLeave}>
                {this.props.children}
            </div>
        );

    }

});