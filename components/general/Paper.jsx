import { Link } from 'react-router';

import React from 'react';

module.exports = React.createClass({

  render() {

    var style = {};

    var border_var = '1px solid rgba(0,0,0,.1)';

    style.border = '0';
    style.backgroundColor = 'white';
    style.border = 0;

    style.boxShadow = '0 1px 1px rgba(0,0,0,.15)';

    if (this.props.style) style = Object.assign({}, style, this.props.style);

    if (this.props.to) {

      // Might screw things up in some cases
      return (
        <Link style={style} to={this.props.to} className={this.props.className} onMouseEnter={this.props.onMouseEnter} onMouseLeave={this.props.onMouseLeave}>
          <div style={style} className={this.props.className}>
            {this.props.children}
          </div>
        </Link>
      );

    }
    else {

      return (
        <div style={style} className={this.props.className} onClick={this.props.onClick} onMouseEnter={this.props.onMouseEnter} onMouseLeave={this.props.onMouseLeave}>
          {this.props.children}
        </div>
      );

    }



  }

});