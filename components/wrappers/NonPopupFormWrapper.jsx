import React from 'react';

import Paper from 'Paper';

import s from 'style_vars';

module.exports = React.createClass({

    render() {

        var style = {
            width: s('width', 'NonPopupFormWrapper'),
            marginTop: s('marginTop', 'NonPopupFormWrapper'),
            marginLeft: 'auto',
            marginRight: 'auto',
            borderRadius: s('borderRadius', 'NonPopupFormWrapper')
        };

        return (
            <Paper style={style}>
                {this.props.children}
            </Paper>
        );

    }

});