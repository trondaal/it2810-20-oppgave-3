import React from 'react';
import hash_it from 'hash_it';

// Not yet in use
import MediaQuery from 'react-responsive';

import Paper from 'Paper';

import s from 'style_vars';

import split_into_Ns from 'split_into_Ns';

module.exports = React.createClass({

    render() {

        var {
            objects,
            rendered_objects,
            component_called_from,
            style_type
        } = this.props;

        if (!objects) throw new Error(`objects missing in ListOfObjectsWrapper called from ${component_called_from}`);
        if (!rendered_objects) throw new Error(`rendered_objects missing in ListOfObjectsWrapper called from ${component_called_from}`);
        if (!component_called_from) throw new Error('component_called_from missing in ListOfObjectsWrapper');

        var keys = objects.map(
            object => hash_it(object)
        );

        var n;
        var percentage_of_horizontal_distance_that_is_space;
        var vertical_distance_between_rows;
        var is_used_for_main_element;
        var width;

        if (component_called_from === 'Options') {

            var longest_text = 0;

            for (var object of objects) {
                if (object.text.length > longest_text) longest_text = object.text.length;
            }

            if (objects.length < 3) {
                n = 2;
                percentage_of_horizontal_distance_that_is_space = 12;
                width = '10%';
            }
            else {
                throw new Error('Update of Options.jsx need.');
            }

        }
        else if (component_called_from === 'BookListContainer') {

            width = '100%';
            n = 3;
            percentage_of_horizontal_distance_that_is_space = 5;
            vertical_distance_between_rows = '16px';

        }
        else if (component_called_from === 'TransferralRequestListContainer') {
            
            width = '100%';
            n = 2;
            percentage_of_horizontal_distance_that_is_space = 2;
            vertical_distance_between_rows = '16px';

        }
        else {

            throw new Error();

        }

        var distances_per_row = n - 1;
        var percentage_space_per_column = 100 / n;
        var percentage_width_of_boxes = (100 - percentage_of_horizontal_distance_that_is_space) / n;
        var percentage_width_of_distances = percentage_of_horizontal_distance_that_is_space / distances_per_row;

        if (!width) throw new Error('width missing')

        var style = {
            width: width,
            display: 'flex',
            flexWrap: 'wrap',
            marginLeft: '0px',
            marginRight: '0px',
            marginTop: '0px',
            marginBottom: '0px',
            paddingLeft: '0px',
            paddingRight: '0px',
            paddingTop: '0px',
            paddingBottom: '0px'
        };

        var boxes_furthest_to_the_left__upper_row = {
            width: `${percentage_width_of_boxes}%`
        };

        var boxes_not_furthest_to_the_left__upper_row = {
            width: `${percentage_width_of_boxes}%`,
            marginLeft: `${percentage_width_of_distances}%`
        };

        var boxes_furthest_to_the_left__not_upper_row = {
            width: `${percentage_width_of_boxes}%`,
            marginTop: vertical_distance_between_rows
        };

        var boxes_not_furthest_to_the_left__not_upper_row = {
            width: `${percentage_width_of_boxes}%`,
            marginLeft: `${percentage_width_of_distances}%`,
            marginTop: vertical_distance_between_rows
        };

        var wrapped_rendered_objects = [];

        split_into_Ns(rendered_objects, n).forEach(
            (row, i) => {
                row.forEach(
                    (box, u) => {
                        if (i === 0) {
                            if (u === 0) {
                                return wrapped_rendered_objects.push(<div style={boxes_furthest_to_the_left__upper_row} key={keys[i * n + u]}>{box}</div>);
                            }
                            else {
                                return wrapped_rendered_objects.push(<div style={boxes_not_furthest_to_the_left__upper_row} key={keys[i * n + u]}>{box}</div>);
                            }
                        }
                        else {
                            if (u === 0) {
                                return wrapped_rendered_objects.push(<div style={boxes_furthest_to_the_left__not_upper_row} key={keys[i * n + u]}>{box}</div>);
                            }
                            else {
                                return wrapped_rendered_objects.push(<div style={boxes_not_furthest_to_the_left__not_upper_row} key={keys[i * n + u]}>{box}</div>);
                            }
                        }
                    }
                )
            }
        );

        return (
            <div style={style}>
                {wrapped_rendered_objects}
            </div>
        );


    }

});