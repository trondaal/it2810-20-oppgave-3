// Libraries
import React from 'react';
import { Link } from 'react-router';
import { push } from 'react-router-redux';

// Forms
import Book from 'Book';

// Components
import Paper from 'Paper';
import FlatButton from 'FlatButton';

import action_creators from 'action_creators';
import push_button from 'push_button';
import post_form from 'post_form';
import transform_form from 'transform_form';
import state_getters_data from 'state_getters_data';
import do_queries_for_container_or_field from 'do_queries_for_container_or_field';
import url_properties_from_location_or_url_str from 'url_properties_from_location_or_url_str';
import s from 'style_vars';
import deduce_width_from_paddings from 'deduce_width_from_paddings';
import numberize_style_var from 'numberize_style_var';
import css from 'css';

var inner_elements = [
    'buy_button',
    'sell_button',
    'delete_button'
];

var outer_elements = [
    'image',
    'div_below_image'
];

var elements = outer_elements.concat(inner_elements);

module.exports = React.createClass({

    onClick() {

        var hovered_element = this.get_hovered_element();

        var {state, obj} = this.props;
        var {isbn, _id} = obj;

        if (hovered_element === 'image' || hovered_element === 'div_below_image') {

            if (isbn) {
                action_creators.PUSH(`/book/${isbn}`);
            }
            else {
                action_creators.PUSH(`/book/${_id}`);
            }

        }
        else if (hovered_element === 'buy_button') {

            var buy_button_form = transform_form(
                Book(),
                'Book_for_buy_button',
                { book_id: _id, isbn: isbn }
            );

            post_form(state, buy_button_form);

            if (isbn) action_creators.PUSH(`/book/${isbn}`);
            else action_creators.PUSH(`/book/${_id}`);

        }
        else if (hovered_element === 'sell_button') {

            var sell_button_form = transform_form(
                Book(),
                'Book_for_sell_button',
                { book_id: _id, isbn: isbn }
            );

            post_form(
                state,
                sell_button_form
            );

            if (isbn) action_creators.PUSH(`/book/${isbn}`);
            else action_creators.PUSH(`/book/${_id}`);

        }
        else if (hovered_element === 'delete_button') {

            var {state, obj} = this.props;

            if (!state) throw new Error();
            if (!obj) throw new Error();

            push_button(
                state,
                {
                    button_type: 'delete_entity',
                    entity_type: 'Book',
                    entity_id: obj._id
                }
            );

        }
        else {
            throw new Error();
        }

    },

    get_hovered_element() {

        var state = this.state;

        if (!state) return null;

        var hovered_inner_element;

        for (var inner_element of inner_elements) {
            if (state.enter_count[inner_element] > state.leave_count[inner_element]) {
                hovered_inner_element = inner_element;
            }
        }

        if (hovered_inner_element) return hovered_inner_element;

        var hovered_outer_element;

        for (var outer_element of outer_elements) {
            if (state.enter_count[outer_element] > state.leave_count[outer_element]) {
                hovered_outer_element = outer_element;
            }
        }

        return hovered_outer_element;

    },

    is_being_hovered() {

        if (this.get_hovered_element()) return true;
        else return false;

    },

    initiatial_state() {

        var state = {
            enter_count: {},
            leave_count: {}
        };

        for (var element of elements) {
            state.enter_count[element] = 0;
            state.leave_count[element] = 0;
        }

        return state;

    },

    onMouseEnter(element) {

        var state;

        if (!this.state) state = this.initiatial_state();
        else state = this.state;

        state.enter_count[element]++;

        this.setState(state);

    },

    onMouseLeave(element) {

        var state = this.state;

        state.leave_count[element]++;

        this.setState(state);

    },

    // This might be done more elegantly using currying.
    // Also, yes, I did try to do all of this in CSS at first, but it was not that straight forward.

    onMouseEnter_image() {
        this.onMouseEnter('image');
    },
    onMouseLeave_image() {
        this.onMouseLeave('image');
    },
    onMouseEnter_div_below_image() {
        this.onMouseEnter('div_below_image');
    },
    onMouseLeave_div_below_image() {
        this.onMouseLeave('div_below_image');
    },
    onMouseEnter_buy_button() {
        this.onMouseEnter('buy_button');
    },
    onMouseLeave_buy_button() {
        this.onMouseLeave('buy_button');
    },
    onMouseEnter_sell_button() {
        this.onMouseEnter('sell_button');
    },
    onMouseLeave_sell_button() {
        this.onMouseLeave('sell_button');
    },
    onMouseEnter_delete_button() {
        this.onMouseEnter('delete_button');
    },
    onMouseLeave_delete_button() {
        this.onMouseLeave('delete_button');
    },

    render() {

        var {obj, state} = this.props;

        var image = obj.image;
        if (!image) image = 'http://d3n8a8pro7vhmx.cloudfront.net/americansforsafeaccess/pages/6835/meta_images/original/Book_logo.png?1394128830';

        var style = {
            borderRadius: s('borderRadius', 'BookBox'),
            textDecoration: 'none',
            backgroundColor: s('backgroundColor', 'BookBox', 'div_below_image')
        }

        var image_div = {
            backgroundImage: `url(${image})`,
            height: s('height', 'BookBox', 'image'),
            width: '100%',
            backgroundSize: 'cover',
            backgroundPosition: '50% 50%',
            borderTopRightRadius: s('borderRadius', 'BookBox'),
            borderTopLeftRadius: s('borderRadius', 'BookBox')
        };

        var div_below_image = {
            height: s('height', 'BookBox', 'div_below_image'),
            width: deduce_width_from_paddings(s('paddingLeftAndRight', 'BookBox')),
            borderBottomRightRadius: s('borderRadius', 'BookBox'),
            borderBottomLeftRadius: s('borderRadius', 'BookBox'),
            paddingTop: s('paddingTopAndBottom', 'BookBox', 'div_below_image'),
            paddingBottom: s('paddingTopAndBottom', 'BookBox', 'div_below_image'),
            paddingLeft: s('paddingLeftAndRight', 'BookBox'),
            paddingRight: s('paddingLeftAndRight', 'BookBox'),
            backgroundColor: this.is_being_hovered() ? s('backgroundColor', 'BookBox', 'hover_div') : null,
            ':hover': {
                cursor: 'pointer'
            }
        };

        var hover_image_div_height = parseInt(s('height', 'BookBox', 'image').replace('px', ''));

        var hover_image_div = {
            height: `${hover_image_div_height}px`,
            marginTop: `-${hover_image_div_height}px`,
            borderRadius: s('borderRadius', 'BookBox'),
            cursor: 'pointer',
            position: 'relative',
            backgroundColor: this.is_being_hovered() ? 'rgba(0, 0, 0, 0.1)' : null
        }

        var title = {
            fontSize: s('fontSize', 'BookBox', 'title'),
            color: s('color', 'BookBox', 'title'),
            marginTop: s('marginTop', 'BookBox', 'title'),
            whiteSpace: "nowrap",
            textOverflow: "ellipsis",
            overflow: "hidden"
        };

        var div_with_buttons = {
            display: 'flex',
            paddingTop: s('paddingTop', 'BookBox', 'div_with_buttons')
        };

        var flat_button = {
            ':nth-child(2)': {
                marginLeft: s('marginLeft', 'BookBox', 'flat_button', ':nth-child(2)')
            },
            ':nth-child(3)': {
                marginLeft: s('marginLeft', 'BookBox', 'flat_button', ':nth-child(2)')
            }
        }

        var delete_book;

        if (state_getters_data.is_admin(state.data)) delete_book = <FlatButton style={flat_button} onMouseEnter={this.onMouseEnter_delete_button} onMouseLeave={this.onMouseLeave_delete_button}>Delete</FlatButton>

        return (

            <div>
                <Paper onClick={this.onClick} style={style}>
                    <div style={image_div} onMouseEnter={this.onMouseEnter_image} onMouseLeave={this.onMouseLeave_image} />
                    <div style={hover_image_div} onMouseEnter={this.onMouseEnter_image} onMouseLeave={this.onMouseLeave_image} />
                    <div className={css(div_below_image)} onMouseEnter={this.onMouseEnter_div_below_image} onMouseLeave={this.onMouseLeave_div_below_image}>
                        <div style={title}>{obj.title}</div>
                        <div className={css(div_with_buttons)}>
                            <FlatButton style={flat_button} onMouseEnter={this.onMouseEnter_buy_button} onMouseLeave={this.onMouseLeave_buy_button}>Buy</FlatButton>
                            <FlatButton style={flat_button} onMouseEnter={this.onMouseEnter_sell_button} onMouseLeave={this.onMouseLeave_sell_button}>Sell</FlatButton>
                            {delete_book}
                        </div>
                    </div>
                </Paper>
            </div>

        );

    }

});