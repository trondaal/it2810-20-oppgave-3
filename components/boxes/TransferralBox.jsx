import React from 'react';

import Paper from 'Paper';

import s from 'style_vars';

module.exports = React.createClass({

    render() {

        var obj = this.props.obj;

        var {buy_or_sell} = obj;
        var {username, email, image, price} = obj.user;
        var {title} = obj.book;

        if (!image) image = 'https://www.cvrc.org/wp-content/uploads/2016/05/index-1.png';

        var height = '70px';

        var style = {
            borderRadius: '2px',
            height: height,
            display: 'flex'
        };

        var image_div = {
            backgroundImage: `url(${image})`,
            width: '20%',
            height: height,
            backgroundSize: 'cover',
            backgroundPosition: '50% 50%',
            borderTopRightRadius: '2px',
            borderBottomRightRadius: '2px'
        };
        var non_image_div = {
            padding: '10px',
            width: '80%',
            display: 'flex',
            alignItems: 'center'
        };

        var text_style = {
            fontSize: 15,
            color: 'rgba(0, 0, 0, 0.85)'
        }

        var html = `<b>${username}</b> wants to ${buy_or_sell} <b>${title}</b>.`;
        if (email) html += ` Get in touch with ${username} on <b>${email}</b>.`;
        else html += ` Unfortunately, ${username} did not add an email, so we don't know how to get in touch.`;

        return (
            <Paper style={style}>
                <div style={non_image_div}>
                    <div style={text_style} dangerouslySetInnerHTML={{ __html: html }} />
                </div>
                <div style={image_div}/>
            </Paper>
        )

    }

});