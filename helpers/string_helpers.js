var ExportsObj = {};

var remove_char_from_start_and_end_if_present = (str, ch) => {

    if (ch.length !== 1) throw new Error('Oops');
    if (str.length === 0) return str;
    if (str.length === 1) {
        if (str.charAt(0) === ch) return '';
        else return str;
    }

    var remove_start = str[0] === ch;
    var remove_end = str[str.length - 1] === ch;

    if (remove_start && remove_end) {
        return str.substr(1, str.length - 2);
    }
    else if (remove_start) {
        return str.substr(1, str.length - 1);
    }
    else if (remove_end) {
        return str.substr(0, str.length - 1);
    }

    return str;

};
ExportsObj.remove_char_from_start_and_end_if_present = remove_char_from_start_and_end_if_present;

module.exports = ExportsObj;