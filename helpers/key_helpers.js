// Libraries:
import hash_it from 'hash_it';

var ExportsObj = {};
var return_name_that_isnt_in_arr = (str, arr) => {

    if (!str) throw Error('No str received');
    if (!arr) throw Error('No arr received');

    var return_str = str;
    var bool = false;

    do {

        if (arr.indexOf(str) == -1) {
            bool = true;
        } else {
            return_str = return_str + '_';
        }

    }
    while (!bool);

    return return_str;

};
ExportsObj.return_name_that_isnt_in_arr = return_name_that_isnt_in_arr;

var add_keys_to_form = (form_obj, form_key) => {

    if (!form_obj) throw Error('No form_obj received');
    if (!form_key) throw Error('No form_key received');

    form_obj.fields.forEach(

        (field, i_1) => {
            
            if (!field.field_key) field.field_key = hash_it(field) + i_1;

            if (field.options) {

                field.options.forEach(

                    (option, i_2) => {
                        if (!option.option_key) option.option_key = hash_it(option) + i_2;
                    }

                )

            }
        
        }
    );

    // TODO: Check for duplicate keys just in case?

    form_obj.form_key = form_key;

    return form_obj;

};
ExportsObj.add_keys_to_form = add_keys_to_form;

module.exports = ExportsObj;