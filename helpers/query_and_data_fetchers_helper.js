import get_data from 'get_data';
import string_helpers from 'string_helpers';


var ExportObj = {};

var url_properties_from_location_or_url_str = (location_or_url_str) => {

    if (!location_or_url_str) throw new Error();

    var location, pathname, search, hash;

    if (typeof location_or_url_str === 'string') {

        var location = {};

        location.pathname = '';
        location.search = '';
        location.hash = '';

        pathname = true;

        for (var char of location_or_url_str) {
            if (char === '?') search = true;
            if (char === '#') hash = true;
            if (pathname) pathname += char;
            if (search) search += char;
            if (hash) hash += char;
        }

    }
    else {
        location = location_or_url_str;
    }

    var pathname = location.pathname;
    var pathname_arr = location.pathname_arr;
    var parameters = location.parameters;
    var search = location.search;

    var pathname = string_helpers.remove_char_from_start_and_end_if_present(pathname, '/');
    var pathname_arr = pathname.split('/');

    var parameters = {};

    if (search) {

        search = search.substring(1);

        var search_arr = search.split('&');

        for (var x of search_arr) {
            var y = x.split('=');
            parameters[y[0]] = y[1];
        }

    };

    if (hash) {
        hash = hash.substring(1);
    };

    return {
        pathname_arr,
        parameters,
        hash,
        pathname
    };

};
ExportObj.url_properties_from_location_or_url_str = url_properties_from_location_or_url_str;

var deal_with_incramentally_loaded_entity = (data, url_properties, key_of_field, entities_per_page) => {

    if (!data ||  typeof data !== 'object') throw new Error();
    if (!url_properties ||  typeof url_properties !== 'object') throw new Error();
    if (typeof key_of_field !== 'string') throw new Error();
    if (typeof entities_per_page !== 'number') throw new Error();

    var page = url_properties.parameters.page;
    if (!page) page = 1;
    page = parseInt(page, 10);

    if (typeof page !== 'number') throw new Error();

    var entities = [];

    for (var i = 1; i <= page; i++) {
        if (data[`${key_of_field}_page_${i}`]) {
            for (var x of data[`${key_of_field}_page_${i}`]) {
                entities.push(x);
            }
        }
    }

    var entity_count = data[`${key_of_field}_count`];

    var display_load_more_button = entity_count > page * entities_per_page;

    return {
        entities,
        display_load_more_button
    };

};
ExportObj.deal_with_incramentally_loaded_entity = deal_with_incramentally_loaded_entity;


module.exports = ExportObj;