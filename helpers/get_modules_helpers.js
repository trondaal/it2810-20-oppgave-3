const fs = require('fs');

// Change the value of these two files when the relevant folders are moved (or this folder is moved one level deeper from the root)
var path_to_folder_with_models = '../form_types/';
var path_to_folder_with_form_types = '../form_types/';


var ExportsObj = {};

var get_names_from_folder = (folder_str) => {

    return fs.readdirSync(folder_str, (err, files) => {
        files.map(file => file);
    });

};

var get_names_of_models = () => {

    return get_names_from_folder(path_to_folder_with_models);

};
ExportsObj.get_names_of_models = get_names_of_models;

var get_models = () => {

    var return_obj = {};

    for (name of get_names_of_models()) {
        return_obj[name] = require(name);
    }

    return return_obj;

};
ExportsObj.get_models = get_models;

var get_names_of_form_types = () => {

    return get_names_from_folder(path_to_folder_with_form_types);

};
ExportsObj.get_names_of_form_types = get_names_of_form_types;

var get_form_types = () => {

};
ExportsObj.get_form_types = get_form_types;