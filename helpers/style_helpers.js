// For updating:
// - https://www.npmjs.com/package/postcss-js
// - http://survivejs.com/react/advanced-techniques/styling-react/
// - http://andrewhfarmer.com/how-to-style-react/

import { StyleSheet, css as css_fun_from_aphrodite } from 'aphrodite';

var ExportsObj = {};

// This function can be given an arbitary number of inputs
// e.g.: process_css(style_obj_1, style_obj_2, ..., style_obj_n)
var css = (style_obj_or_arr_of_style_objects, test_input) => {

    var thingies_with_semicolons = {};

    if (test_input) throw new Error('If you want to process several arguments, pass them as an array.');

    var merged_styles = {};

    if (style_obj_or_arr_of_style_objects.constructor === Array) {

        var style_objects = style_obj_or_arr_of_style_objects;

        style_objects = style_objects.filter(x => x !== null);

        for (var style_object of style_objects) {
            for (var key in style_object) {
                if (key.includes(':')) {
                    if (!thingies_with_semicolons[key]) thingies_with_semicolons[key] = [];
                    thingies_with_semicolons[key].push(style_object[key]);
                }
            }
        }

        for (var style_object of style_objects) {
            Object.assign(merged_styles, style_object);
        }

        for (var key in merged_styles) {
            if (thingies_with_semicolons[key]) {
                if (thingies_with_semicolons[key].length === 1) {
                    merged_styles[key] = thingies_with_semicolons[key][0];
                }
                else if (thingies_with_semicolons[key].length > 1) {
                    var obj = {};
                    for (var nested_thingie of thingies_with_semicolons[key]) {
                        obj = Object.assign(obj, nested_thingie);
                    }
                    merged_styles[key] = obj;
                }
                else {
                    throw new Error('Huh..');
                }

            }
        }


    }
    else {

        merged_styles = style_obj_or_arr_of_style_objects;

    }

    var stylesheet = StyleSheet.create({
        obj_key_because_of_reasons: merged_styles
    });

    return css_fun_from_aphrodite(stylesheet.obj_key_because_of_reasons)

};
ExportsObj.css = css;

var numberize_style_var = (style_var) => {

    if (typeof style_var === 'number') return style_var;

    if (!style_var.includes('%') && !style_var.includes('px')) throw new Error('This type of input is not currently accounted for.');

    style_var = style_var.replace('px', '');
    style_var = style_var.replace('%', '');

    return parseInt(style_var);

};
ExportsObj.numberize_style_var = numberize_style_var;

var fetch_style_vars = (Obj, property, component_referant_or_other_top_level_referant, style_obj_referant, selector_referant, return_number_str) => {

    var throw_error = (extra_message) => {
        throw new Error(
            `Error in style_vars.js / styler_helpers.js. Inputs: ${property}, ${component_referant_or_other_top_level_referant}, ${style_obj_referant}, ${selector_referant}. Extra message: ${extra_message}`
        );
    };

    if (style_obj_referant && !selector_referant && style_obj_referant.includes(':')) {
        selector_referant = style_obj_referant;
        style_obj_referant = null;
    }

    if (style_obj_referant === 'return number') {
        return_number_str = 'return number';
        style_obj_referant = null;
    }

    if (selector_referant === 'numberize') {
        return_number_str = 'return number';
        selector_referant = null;
    }

    var return_var = null;

    if (!property || typeof property !== 'string') throw_error();
    if (!component_referant_or_other_top_level_referant || typeof component_referant_or_other_top_level_referant !== 'string') throw_error();
    if (style_obj_referant && typeof style_obj_referant !== 'string') throw_error('if (style_obj_referant && typeof style_obj_referant !== \'string\')');
    if (selector_referant && typeof selector_referant !== 'string') throw_error('if (selector_referant && typeof selector_referant !== \'string\')');
    if (style_obj_referant === '') throw_error('if (style_obj_referant === \'\')');
    if (selector_referant === '') throw_error('if (selector_referant === \'\')');
    if (selector_referant && selector_referant.charAt(0) !== ':') throw_error(`${selector_referant} should have stared with a semicolon`)

    var obj__property = Obj[property];
    var obj_or_str__component_referant_or_other_top_level_referant;
    var obj_or_str__style_obj_referant;
    var str__selector_referant;

    if (!obj__property) throw new Error(`The property ${property} cannot be found in style_vars.js`);

    obj_or_str__component_referant_or_other_top_level_referant = obj__property[`_${component_referant_or_other_top_level_referant}`];
    if (!obj_or_str__component_referant_or_other_top_level_referant) {
        obj_or_str__component_referant_or_other_top_level_referant = obj__property[component_referant_or_other_top_level_referant];
    }

    if (!obj_or_str__component_referant_or_other_top_level_referant) {
        throw new Error(`The component or referent ${component_referant_or_other_top_level_referant} cannot be found in ${property} in style_vars.js`);
    }

    obj_or_str__style_obj_referant = obj_or_str__component_referant_or_other_top_level_referant[`_${style_obj_referant}`];
    if (!obj_or_str__style_obj_referant) {
        obj_or_str__style_obj_referant = obj_or_str__component_referant_or_other_top_level_referant[style_obj_referant];
    }

    if (obj_or_str__style_obj_referant) {
        str__selector_referant = obj_or_str__style_obj_referant[selector_referant];
    }
    else {
        str__selector_referant = obj_or_str__component_referant_or_other_top_level_referant[selector_referant];
    }

    // style_obj_referant AND selector_referant
    if (style_obj_referant && selector_referant) {
        if (typeof str__selector_referant !== 'string') throw_error('if (style_obj_referant && selector_referant) {');
        return return_number_str ? numberize_style_var(str__selector_referant) : str__selector_referant;
    }
    
    // style_obj_referant but NOT selector_referant
    if (style_obj_referant && !selector_referant) {
        if (typeof obj_or_str__style_obj_referant !== 'string') {
            obj_or_str__style_obj_referant = obj_or_str__component_referant_or_other_top_level_referant[style_obj_referant]
            if (typeof obj_or_str__style_obj_referant !== 'string') {
                throw_error('if (style_obj_referant && !selector_referant)');
            }
        }
        return return_number_str ? numberize_style_var(obj_or_str__style_obj_referant) : obj_or_str__style_obj_referant;
    }

    // NOT style_obj_referant but selector_referant
    if (!style_obj_referant && selector_referant) {
        if (typeof str__selector_referant !== 'string') throw_error('if (style_obj_referant && selector_referant) {');
        return return_number_str ? numberize_style_var(str__selector_referant) : str__selector_referant;
    }

    // NOT style_obj_referant and NOT selector_referant
    if (!style_obj_referant && !selector_referant) {
        if (typeof obj_or_str__component_referant_or_other_top_level_referant !== 'string') {
            obj_or_str__component_referant_or_other_top_level_referant = obj__property[component_referant_or_other_top_level_referant];
            if (typeof obj_or_str__component_referant_or_other_top_level_referant !== 'string') throw_error('if (typeof obj_or_str__component_referant_or_other_top_level_referant !== \'string\')');
        }
        return return_number_str ? numberize_style_var(obj_or_str__component_referant_or_other_top_level_referant) : obj_or_str__component_referant_or_other_top_level_referant;
    }

    throw_error('This is probably Tor\'s fault.');

};
ExportsObj.fetch_style_vars = fetch_style_vars;

// Taken from here: http://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors
var sbcRip;
var blend_colors_by_percentage = (from, to, percentage) => {

    if (typeof (percentage) != 'number' || percentage < -1 || percentage > 1 || typeof (from) != 'string' || (from[0] != 'r' && from[0] != '#') || (typeof (to) != 'string' && typeof (to) != 'undefined')) {
        throw new Error('Oops');
    }
    if (!sbcRip) sbcRip = (d) => {
        var l = d.length, RGB = {};
        if (l > 9) {
            d = d.split(',');
            if (d.length < 3 || d.length > 4) {
                throw new Error('Oops');
            }
            RGB[0] = i(d[0].slice(4)), RGB[1] = i(d[1]), RGB[2] = i(d[2]), RGB[3] = d[3] ? parseFloat(d[3]) : -1;
        } else {
            if (l == 8 || l == 6 || l < 4) {
                throw new Error('Oops');
            }
            if (l < 6) {
                d = '#' + d[1] + d[1] + d[2] + d[2] + d[3] + d[3] + (l > 4 ? d[4] + '' + d[4] : '');
            }
            d = i(d.slice(1), 16), RGB[0] = d >> 16 & 255, RGB[1] = d >> 8 & 255, RGB[2] = d & 255, RGB[3] = l == 9 || l == 5 ? r(((d >> 24 & 255) / 255) * 10000) / 10000 : -1;
        }
        return RGB;
    }
    var i = parseInt, r = Math.round, h = from.length > 9, h = typeof (to) == 'string' ? to.length > 9 ? true : to == 'c' ? !h : false : h, b = percentage < 0, percentage = b ? percentage * -1 : percentage, to = to && to != 'c' ? to : b ? '#000000' : '#FFFFFF', f = sbcRip(from), t = sbcRip(to);
    if (!f || !t) {
        throw new Error('Oops');
    }
    if (h) {
        return 'rgb(' + r((t[0] - f[0]) * percentage + f[0]) + ',' + r((t[1] - f[1]) * percentage + f[1]) + ',' + r((t[2] - f[2]) * percentage + f[2]) + (f[3] < 0 && t[3] < 0 ? ')' : ',' + (f[3] > -1 && t[3] > -1 ? r(((t[3] - f[3]) * percentage + f[3]) * 10000) / 10000 : t[3] < 0 ? f[3] : t[3]) + ')');
    }
    else {
        return '#' + (0x100000000 + (f[3] > -1 && t[3] > -1 ? r(((t[3] - f[3]) * percentage + f[3]) * 255) : t[3] > -1 ? r(t[3] * 255) : f[3] > -1 ? r(f[3] * 255) : 255) * 0x1000000 + r((t[0] - f[0]) * percentage + f[0]) * 0x10000 + r((t[1] - f[1]) * percentage + f[1]) * 0x100 + r((t[2] - f[2]) * percentage + f[2])).toString(16).slice(f[3] > -1 || t[3] > -1 ? 1 : 3);
    }

};
ExportsObj.blend_colors_by_percentage = blend_colors_by_percentage;

var darken_color_by_percentage = (color, percentage) => {

    return blend_colors_by_percentage(color, 'rgb(0, 0, 0)', percentage*0.01);

};
ExportsObj.darken_color_by_percentage = darken_color_by_percentage;

var lighten_color_by_percentage = (color, percentage) => {

    return blend_colors_by_percentage(color, 'rgb(255, 255, 255)', percentage*0.01);

};
ExportsObj.lighten_color_by_percentage = lighten_color_by_percentage;

var deduce_width_from_paddings = (horizontal_padding_1, horizontal_padding_2) => {

    if (!horizontal_padding_2) horizontal_padding_2 = horizontal_padding_1;

    if (!horizontal_padding_1.includes('%')) throw new Error('This type of input is not currently accounted for.');
    if (!horizontal_padding_2.includes('%')) throw new Error('This type of input is not currently accounted for.');

    horizontal_padding_1 = horizontal_padding_1.replace('%', '');
    horizontal_padding_2 = horizontal_padding_2.replace('%', '');

    return `${100-horizontal_padding_1-horizontal_padding_2}%`;

};
ExportsObj.deduce_width_from_paddings = deduce_width_from_paddings;

module.exports = ExportsObj;