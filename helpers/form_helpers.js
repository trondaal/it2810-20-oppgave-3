// Helpers:
import key_helpers from 'key_helpers';

// Libraries:
import hash_it from 'hash_it';

var ExportsObj = {};

var form_structure = {

    forms__possible_fields_and_their_data_types: {
        fields: 'array',
        queries_on_response: 'array',
        form_key: 'string',
        entity_type: 'string',
        id_of_entity: 'string',
        form_type: 'string',
        post_button_text: 'string',
        action_type: 'string',
        text: 'string',
        extra_text: 'string',
        extra_extra_text: 'string'
    },

    forms__obligatory_fields: [
        'fields',
        'form_type'
    ],

    fields__possible_fields_and_their_data_types: {
        pre_change_value: 'anything',
        value: 'anything',
        options: 'array',
        initial_dropdown_options: 'array',
        queries: 'array',
        is_password_field: 'boolean',
        dont_display: 'boolean',
        min_number_of_selections_allowed: 'number',
        max_number_of_selections_allowed: 'number',
        field_key: 'string',
        text: 'string',
        extra_text: 'string',
        extra_extra_text: 'string',
        field_name: 'string',
        field_type: 'string',
        data_type_of_value: 'string'
    },

    fields__obligatory_fields: [
        'field_key'
    ],

    fields_of_specific_type__obligatory_fields: {
        textfield: [],
        options: [
            'min_number_of_selections_allowed',
            'max_number_of_selections_allowed',
            'options'
        ]
    },

    options__possible_fields_and_their_data_types: {
        value: 'anything',
        is_input_field: 'boolean',
        is_selected: 'boolean',
        option_key: 'string',
        text: 'string',
        extra_text: 'string',
        extra_extra_text: 'string',
        data_type_of_value: 'string'
    },

    options__obligatory_fields: [
        'value'
    ]

};

var check_fields_of_obj = (obj, data_types_for_fields_in_obj) => {

    if (!obj) throw new Error('No obj provided');
    if (!data_types_for_fields_in_obj) throw new Error('No data_types_for_fields_in_obj provided');

    for (var key of Object.keys(obj)) {
        if (!data_types_for_fields_in_obj[key]) throw new Error(`The field ${key} seems to be wrongly named or spelled, or to not be specified as allowed.`);

        if (data_types_for_fields_in_obj[key] === 'string') {

            if (typeof obj[key] !== 'string') {
                throw new Error(
                    `According to the form-structure-specification ${key} should be a string, but \'typeof\' of ${obj[key]} returns ${typeof obj[key]}`
                );
            }

        }
        else if (data_types_for_fields_in_obj[key] === 'number') {

            if (typeof obj[key] !== 'number') {
                throw new Error(
                    `According to the form-structure-specification ${key} should be a number, but \'typeof\' of ${obj[key]} returns ${typeof obj[key]}`
                );
            }

        }
        else if (data_types_for_fields_in_obj[key] === 'boolean') {

            if (typeof obj[key] !== 'boolean') {
                throw new Error(
                    `According to the form-structure-specification ${key} should be a boolean, but \'typeof\' of ${obj[key]} returns ${typeof obj[key]}`
                );
            }

        }
        else if (data_types_for_fields_in_obj[key] === 'object') {

            if (typeof obj[key] !== 'object' || obj[key].constructor === Array) {
                throw new Error(
                    `According to the form-structure-specification ${key} should be a object (also, arrays are not regarded to be objects here)`
                );
            }

        }
        else if (data_types_for_fields_in_obj[key] === 'array') {

            if (typeof obj[key] !== 'object' || obj[key].constructor !== Array) {
                throw new Error(
                    `According to the form-structure-specification ${key} should be a array`
                );
            }

        }
        else if (data_types_for_fields_in_obj[key] !== 'anything') {

            throw new Error(
                `The data_type set for ${key} seems to not be among the ones that are allowed/checked`
            );

        }
    }

};

var check_option_for_errors = (option) => {

    check_fields_of_obj(option, form_structure.options__possible_fields_and_their_data_types);

    for (var key_of_obligatory_field in form_structure.options__obligatory_fields) {
        var obligatory_field = form_structure.options__obligatory_fields[key_of_obligatory_field];
        if (!option[obligatory_field]) throw new Error(`Obligatory field ${obligatory_field} missing in option.`);
    }

};

var check_field_for_errors = (field) => {

    check_fields_of_obj(field, form_structure.fields__possible_fields_and_their_data_types);

    for (var obligatory_field of form_structure.fields__obligatory_fields) {
        if (!field[obligatory_field]) throw new Error(`Obligatory field ${obligatory_field} missing in field.`);
    }

    for (var key_of_obligatory_field in form_structure.fields_of_specific_type__obligatory_fields[field.field_type]) {
        var obligatory_field = form_structure.fields_of_specific_type__obligatory_fields[field.field_type][key_of_obligatory_field];
        if (!field[obligatory_field]) throw new Error(`Obligatory field  ${field[obligatory_field]} missing in field.`);
    }

    if (field.options) {
        for (var option of field.options) {
            check_option_for_errors(option);
        }
    }

};

var check_form_for_errors = (form) => {

    check_fields_of_obj(form, form_structure.forms__possible_fields_and_their_data_types);

    for (var obligatory_field of form_structure.forms__obligatory_fields) {
        if (!form[obligatory_field]) throw new Error(`Obligatory field ${obligatory_field} missing in form.`);
    }

    for (var field of form.fields) {
        check_field_for_errors(field);
    }

};
ExportsObj.check_form_for_errors = check_form_for_errors;

var initiate_form = (form_obj, form_key) => {

    if (!form_obj) throw Error('No form_obj received');
    if (!form_key) throw Error('No form_key received');

    form_obj = key_helpers.add_keys_to_form(form_obj, form_key);

    check_form_for_errors(form_obj);

    return form_obj;

};
ExportsObj.initiate_form = initiate_form;


var change_field = (form_obj, field_name, field_key_to_edit, new_value) => {

    var field = form_obj.fields.find(
        x => x.field_name === field_name
    );

    if (!field) throw new Error();

    field[field_key_to_edit] = new_value;

};


var transform_form = (form_obj, transformation_type, data_obj) => {

    var form_type = form_obj.form_type;

    if (transformation_type === 'Book_for_buy_button') {

        if (form_type !== 'Book') throw new Error();
        if (!data_obj) {
            console.log({form_obj, transformation_type, data_obj});
            throw new Error();
        }

        change_field(form_obj, 'isbn', 'value', data_obj.isbn);
        change_field(form_obj, 'book_id', 'value', data_obj.book_id);
        change_field(form_obj, 'buy_or_sell', 'value', 'buy');

    }
    else if (transformation_type === 'Book_for_sell_button') {

        if (form_type !== 'Book') throw new Error();
        if (!data_obj) {
            console.log({form_obj, transformation_type, data_obj});
            throw new Error();
        }

        change_field(form_obj, 'isbn', 'value', data_obj.isbn);
        change_field(form_obj, 'book_id', 'value', data_obj.book_id);
        change_field(form_obj, 'buy_or_sell', 'value', 'sell');

    }
    else if (transformation_type === 'Book_sell_book') {

        change_field(form_obj, 'title', 'text', 'What is the title of the book you want to sell?');
        change_field(form_obj, 'buy_or_sell', 'value', 'sell');
        change_field(form_obj, 'buy_or_sell', 'dont_display', true);
        
    }
    else if (transformation_type === 'Book_buy_book') {

        change_field(form_obj, 'title', 'text', 'What is the title of the book you want to buy?');
        change_field(form_obj, 'buy_or_sell', 'value', 'buy');
        change_field(form_obj, 'buy_or_sell', 'dont_display', true);
        
    }
    else {

        throw new Error();

    }

    return form_obj;

};
ExportsObj.transform_form = transform_form;





module.exports = ExportsObj;