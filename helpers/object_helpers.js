let sha1 = require('sha1');

let ExportsObj = {};


// From here: http://stackoverflow.com/questions/5612787/converting-an-object-to-a-string
let object_to_string = (obj) => {

    let string = [];

    if (obj == undefined) {
        return String(obj);
    }
    else if (typeof (obj) == 'object' && (obj.join == undefined)) {
        for (let prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                string.push(prop + ': ' + object_to_string(obj[prop]));
            }
        };
        return `{${string.join(',')}}'`;

    }
    else if (typeof (obj) == 'object' && !(obj.join == undefined)) {
        for (let prop in obj) {
            string.push(object_to_string(obj[prop]));
        }
        return `[${string.join(',')}]'`;
    }
    else if (typeof (obj) == 'function') {
        string.push(obj.toString())

    }
    else {
        string.push(JSON.stringify(obj))
    }

    return string.join(',');

};
ExportsObj.object_to_string = object_to_string;

var hash_it_counter = 0;
var hash_it_last_start = 0;

let hash_it = (variable) => {

    // return '' + hash_it_external(variable);

    if (typeof variable !== 'object') {
        return sha1(variable);
    }

    let arr_of_processed_stuff = [];
    let arr_for_further_processing;

    if (variable.constructor === Array) {
        arr_for_further_processing = [
            [
                ['[!top_arr!]'],
                variable
            ]
        ];
    }
    else {
        arr_for_further_processing = [
            [
                ['[!top_obj!]'],
                variable
            ]
        ];
    }

    while (arr_for_further_processing.length > 0) {

        let element_from_arr_for_further_processing = arr_for_further_processing.pop();
        let arr_documenting_nesting = element_from_arr_for_further_processing[0];
        let arr_or_obj = element_from_arr_for_further_processing[1];

        let arr;
        let obj;

        if (arr_or_obj.constructor === Array) {

            arr = arr_or_obj;

            let index = 0;

            arr.forEach(

                x => {

                    let arr_documenting_nesting__copied = arr_documenting_nesting.slice();

                    if (x === null) {
                        // Do nothing
                    }
                    else if (x === undefined) {
                        // Do nothing
                    }
                    else if (x === '') {
                        // Do nothing
                    }
                    else if (Object.is(x, [])) {
                        // Do nothing
                    }
                    else if (Object.is(x, {})) {
                        // Do nothing
                    }
                    else if (typeof x === 'number') {
                        arr_documenting_nesting__copied.push(`[num_with_index_${index}_${x.toString()}]`);
                        arr_of_processed_stuff.push(arr_documenting_nesting__copied);
                    }
                    else if (typeof x === 'boolean') {
                        arr_documenting_nesting__copied.push(`[bool_with_index_${index}_${x.toString()}]`);
                        arr_of_processed_stuff.push(arr_documenting_nesting__copied);
                    }
                    else if (typeof x === 'string') {
                        arr_documenting_nesting__copied.push(`[str_with_index_${index}_${x}]`);
                        arr_of_processed_stuff.push(arr_documenting_nesting__copied);
                    }
                    else if (typeof x === 'object' && x.constructor !== Array) {
                        arr_documenting_nesting__copied.push(`[obj_with_index_${index}]`);
                        arr_for_further_processing.push([arr_documenting_nesting__copied, x]);
                    }
                    else if (typeof x === 'object' && x.constructor === Array) {
                        arr_documenting_nesting__copied.push(`[arr_with_index_${index}]`);
                        arr_for_further_processing.push([arr_documenting_nesting__copied, x]);
                    }
                    else {
                        throw new Error('Oops');
                    }

                }

            );

        }
        else {

            obj = arr_or_obj;

            let arr_documenting_nesting = element_from_arr_for_further_processing[0];
            let obj = element_from_arr_for_further_processing[1];

            Object.keys(obj).forEach(

                key => {

                    let x = obj[key];
                    let arr_documenting_nesting__copied = arr_documenting_nesting.slice();

                    if (x === null) {
                        // Do nothing
                    }
                    else if (x === undefined) {
                        // Do nothing
                    }
                    else if (x === '') {
                        // Do nothing
                    }
                    else if (Object.is(x, [])) {
                        // Do nothing
                    }
                    else if (Object.is(x, {})) {
                        // Do nothing
                    }
                    else if (typeof x === 'number') {
                        arr_documenting_nesting__copied.push(`[${key}:num:${x.toString()}]`);
                        arr_of_processed_stuff.push(arr_documenting_nesting__copied);
                    }
                    else if (typeof x === 'boolean') {
                        arr_documenting_nesting__copied.push(`[${key}:bool:${x.toString()}]`);
                        arr_of_processed_stuff.push(arr_documenting_nesting__copied);
                    }
                    else if (typeof x === 'string') {
                        arr_documenting_nesting__copied.push(`[${key}:str:${x}]`);
                        arr_of_processed_stuff.push(arr_documenting_nesting__copied);
                    }
                    else if (typeof x === 'object' && x.constructor !== Array) {
                        arr_documenting_nesting__copied.push(`[${key}:obj]`);
                        arr_for_further_processing.push([arr_documenting_nesting__copied, x]);

                    }
                    else if (typeof x === 'object' && x.constructor === Array) {
                        arr_documenting_nesting__copied.push(`[${key}:arr]`);
                        arr_for_further_processing.push([arr_documenting_nesting__copied, x]);
                    }
                    else {
                        throw new Error('Oops');
                    }

                }

            );

        }
    }

    return sha1(
        arr_of_processed_stuff
            .map(x => x.join())
            .sort()
            .join()
    );


};
ExportsObj.hash_it = hash_it;


module.exports = ExportsObj;