var ExportsObj = {};

var split_into_Ns = (arr, n) => {

    var return_arr = [];
    var newest_addition_arr = [];

    var counter = 0;

    for (var element of arr) {

        counter++;
        newest_addition_arr.push(element);

        if (counter === n) {
            return_arr.push(newest_addition_arr);
            newest_addition_arr = [];
            counter = 0;
        }

    }

    if (newest_addition_arr.length > 0) return_arr.push(newest_addition_arr);

    return return_arr;

};
ExportsObj.split_into_Ns = split_into_Ns;

module.exports = ExportsObj;