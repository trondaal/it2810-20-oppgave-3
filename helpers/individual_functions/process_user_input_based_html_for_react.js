import sanitizer_helpers from 'sanitizer_helpers';

module.exports = sanitizer_helpers.process_user_input_based_html_for_react;