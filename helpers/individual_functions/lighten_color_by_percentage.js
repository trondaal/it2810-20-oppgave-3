import style_helpers from 'style_helpers';

module.exports = style_helpers.lighten_color_by_percentage;