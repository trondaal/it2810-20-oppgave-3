import style_helpers from 'style_helpers';

module.exports = style_helpers.deduce_width_from_paddings;