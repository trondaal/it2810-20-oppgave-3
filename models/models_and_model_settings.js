var mongoose = require('mongoose');
var Schema = mongoose.Schema;



var ExportObj = {
    model_names: [],
    model_settings: {}
};




// User

var User = new Schema({
    username: String,
    email: String,
    image: String,
    is_admin: Boolean,
    hashed_password: String,
    login_cookies: [String],
    dates_login_cookies_were_added: [Schema.Types.Mixed]
});

var User_settings = {
    //Field-settings
    username: {
        display_setting: 'display_by_default'
    },
    email: {
        display_setting: 'display_by_default'
    },
    image: {
        display_setting: 'display_by_default'
    },
    hashed_password: {
        display_setting: 'hide_by_default'
    },
    login_cookies: {
        display_setting: 'hide_by_default'
    },
    dates_login_cookies_were_added: {
        display_setting: 'hide_by_default'
    },
    is_admin: {
        display_setting: 'hide_by_default'
    }
};

ExportObj['User'] = mongoose.model('User', User, 'User');
ExportObj.model_settings['User'] = User_settings;
ExportObj.model_names.push('User');




// Book

var Book = new Schema({
    title: {type: String, text: true},
    title__autocomplete: [String],
    image: String,
    isbn: String,
    description: String,
    read_more_url: String,
    added_by: { type: Schema.ObjectId, ref: 'User' }
});

var Book_settings = {
    //Field-settings
    title: {
         display_setting: 'display_by_default'
    },
    title__autocomplete: {
         display_setting: 'hide_by_default'
    },
    image: {
        display_setting: 'display_by_default'
    },
    isbn: {
        display_setting: 'display_by_default'
    },
    description: {
        display_setting: 'display_by_default'
    },
    read_more_url: {
        display_setting: 'display_by_default'
    },
    added_by: {
        display_setting: 'display_by_default'
    }
};

ExportObj['Book'] = mongoose.model('Book', Book, 'Book');
ExportObj.model_settings['Book'] = Book_settings;
ExportObj.model_names.push('Book');

Book.index({ title: 'text'});




// Transferral_request

var Transferral_request = new Schema({
    user_ref: { type: Schema.ObjectId, ref: 'User' },
    book_ref: { type: Schema.ObjectId, ref: 'Book' },
    book: Schema.Types.Mixed,
    user: Schema.Types.Mixed,
    book_ref__by_isbn: String,
    buy_or_sell: String
});

var Transferral_request_settings = {
    //Field-settings
    book_ref: {
        display_setting: 'display_by_default'
    },
    book_ref__by_isbn: {
        display_setting: 'display_by_default'
    },
    user_ref: {
        display_setting: 'display_by_default'
    },
    buy_or_sell: {
        display_setting: 'display_by_default'
    },
    user: {
        display_setting: 'display_by_default'
    },
    book: {
        display_setting: 'display_by_default'
    }
};

ExportObj['Transferral_request'] = mongoose.model('Transferral_request', Transferral_request, 'Transferral_request');
ExportObj.model_settings['Transferral_request'] = Transferral_request_settings;
ExportObj.model_names.push('Transferral_request');


// API_query

var API_query = new Schema({
    query_url: String,
    query_result: Schema.Types.Mixed,
    time_added: Date
});

var API_query_settings = {};

ExportObj['API_query'] = mongoose.model('API_query', API_query, 'API_query');
ExportObj.model_settings['API_query'] = API_query_settings;
ExportObj.model_names.push('API_query');



module.exports = ExportObj;