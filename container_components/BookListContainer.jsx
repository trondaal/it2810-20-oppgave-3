// Libraries:
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';

// Components:
import Form from 'Form';
import BookBox from 'BookBox';
import LargeFlatButton from 'LargeFlatButton';
import ListOfObjectsWrapper from 'ListOfObjectsWrapper';

// Helpers:
import state_getters_data from 'state_getters_data';
import global_vars_public from 'global_vars_public';
import do_queries_for_container_or_field from 'do_queries_for_container_or_field';
import url_properties_from_location_or_url_str from 'url_properties_from_location_or_url_str';
import deal_with_incramentally_loaded_entity from 'deal_with_incramentally_loaded_entity';
import action_creators from 'action_creators';
import hash_it from 'hash_it';
import get_data from 'get_data';
import get_queries from 'get_queries';


var Container = React.createClass({

    on_load_more_nutton_click() {

        var {location, state} = this.props;
        var url_properties = url_properties_from_location_or_url_str(location);

        var page = url_properties.parameters.page;
        if (!page) page = 1;
        page = parseInt(page, 10);
        page++;
        url_properties.parameters.page = page;

        var load_more_url = `${url_properties.pathname}?page=${page}`;
        action_creators.PUSH(load_more_url);

        do_queries_for_container_or_field(
            state,
            false,
            url_properties,
            null
        );
        
    },

    render() {

        var {state, location} = this.props;
        var elements_per_page = 3;

        var url_properties = url_properties_from_location_or_url_str(location);
        var {parameters, hash, pathname} = url_properties; 

        if (!state) throw new Error();
        if (!location) throw new Error();

        var data = get_data.for_url(state, url_properties);

        var {entities, display_load_more_button} = deal_with_incramentally_loaded_entity(data, url_properties, 'books', 9);

        var load_more_button;

        if (display_load_more_button) {

            var load_button_style = {
                fontSize: 16,
                textAlign: 'center',
                width: '100%'
            };

            load_more_button = <LargeFlatButton onClick={this.on_load_more_nutton_click}>More...</LargeFlatButton>

        }

        var rendered_objects_for_wrapper = entities.map(
            entity => <BookBox obj={entity} state={state} />
        );

        var keys = entities.map(
            entity => hash_it(entity)
        );

        var style = {
            paddingTop: '60px',
            paddingBottom: '150px',
            marginLeft: 'auto',
            marginRight: 'auto',
            width: '960px'
        };

        // console.log(this.props.state);

        return (
            <div style={style}>
                <ListOfObjectsWrapper
                    objects={entities}
                    rendered_objects={rendered_objects_for_wrapper}
                    component_called_from={'BookListContainer'} />
                {load_more_button}
            </div >
        );

    },

    componentDidMount() {

        var {location, state} = this.props;

        if (!state) throw new Error();
        if (!location) throw new Error();

        do_queries_for_container_or_field(
            state,
            false,
            url_properties_from_location_or_url_str(location),
            null
        );

    }

});

var mapStateToProps = (state) => {
    return { state: state }
}

var mapDispatchToProps = (dispatch) => {
    return { dispatch: dispatch }
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(Container);