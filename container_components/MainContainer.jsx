// Libraries:
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import cookie_jeep from 'cookie-jeep';

import hash_it from 'hash_it';

// Icons
import ExitIcon from 'react-icons/lib/md/close';

// Components:
import Navbar from 'Navbar';
import Form from 'Form';
import Paper from 'Paper';

// State getters:
import state_getters_forms_and_fields from 'state_getters_forms_and_fields';

import action_creators from 'action_creators';
import s from 'style_vars';
import css from 'css';

// Queries and data for container
import get_data from 'get_data';
import get_queries from 'get_queries';

// TODO: Add key-handlers https://github.com/ayrton/react-key-handler 
// (so that forms can be closed by pressing ESC, etc)

var MainContainer = React.createClass({

    render() {

        var {state, location} = this.props;
        var {pathname, search, hash} = location;

        var style = {
            backgroundColor: s('backgroundColor', 'GLOBAL'),
            fontFamily: s('fontFamily', 'GLOBAL'),
            minHeight: '110vh'
        };

        var user_info = state.data.user_info;

        if (!user_info && !state.other.first_render_has_been_completed) {
            var user_info_str = cookie_jeep.read('user_info');
            if (user_info_str) {
                user_info = JSON.parse(user_info_str);
                action_creators.UPDATE_TO_LOGGED_IN(user_info);
            }
        }
        
        var rendered_navbar = <Navbar state={state} location={location} />;

        var rendered_dialog;

        if (state.forms_and_fields.key_of_current_dialog) {

            var rendered_form_or_other_stuff;

            var icon_size = 35;
            var width_form_wrapper;

            var current_form = state_getters_forms_and_fields.get_form(
                state.forms_and_fields, state.forms_and_fields.key_of_current_dialog
            );

            if (state.forms_and_fields.key_of_current_dialog === 'LogIn') {

                width_form_wrapper = 600;

                rendered_form_or_other_stuff = [
                    <Form obj={current_form} state={this.props.state} key={'key1'} />
                ];

            }
            else if (state.forms_and_fields.key_of_current_dialog === 'Register') {

                width_form_wrapper = 600;

                rendered_form_or_other_stuff = [
                    <Form obj={current_form} state={this.props.state} key={'key1'} location={location} />
                ];

            }
            else if (Object.keys(state.forms_and_fields.forms__by_form_key).find(x => x === state.forms_and_fields.key_of_current_dialog)) {

                width_form_wrapper = 800;

                rendered_form_or_other_stuff = <Form obj={current_form} state={this.props.state} location={location} />;

            }
            else {
                throw new Error();
            }

            var s_dialog_div = {
                backgroundColor: 'rgba(0, 0, 0, 0.8)',
                height: '100%',
                width: '100%',
                top: '0',
                zIndex: 10,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                position: 'fixed',
                flexDirection: 'row'
            };

            var s_form_wrapper = {
                width: `${width_form_wrapper}px`,
                borderRadius: '2px',
                maxHeight: '85vh',
                backgroundColor: '#FAFAFA',
                overflow: 'auto'
            };

            var s_icon = {
                zIndex: 20,
                cursor: 'pointer',
                color: 'rgba(255, 255, 255, 0.9)',
                position: 'absolute',
                right: '13px',
                top: '13px',
                ':hover': {
                    color: 'rgba(255, 255, 255, 1)',
                }
            };

            rendered_dialog = (
                <div style={s_dialog_div}>
                    <Paper style={s_form_wrapper}>
                        {rendered_form_or_other_stuff}
                    </Paper>
                    <ExitIcon
                        onClick={() => action_creators.CHANGE_KEY_OF_CURRENT_DIALOG(null, true)}
                        size={icon_size}
                        className={css(s_icon)} />
                </div>
            );

        }

        return (
            <div style={style} onKeyUp={this.onKeyDown}>
                {rendered_navbar}
                <div style={{ marginTop: rendered_navbar ? s('height', 'Navbar') : 0 }}>{this.props.children}</div>
                {rendered_dialog}
            </div>
        );

    },

    componentDidMount() {

        get_queries.that_should_be_sent_for_main_container(this.props.state); // <-- ?
        action_creators.FIRST_RENDER_HAS_BEEN_COMPLETED();

    }

});

var mapStateToProps = (state) => {
    return { state: state }
}

var mapDispatchToProps = (dispatch) => {
    return { dispatch: dispatch }
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(MainContainer);