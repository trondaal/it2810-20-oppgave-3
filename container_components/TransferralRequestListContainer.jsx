// Libraries:
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import hash_it from 'hash_it';
import { Link } from 'react-router';
import PieChart from 'react-simple-pie-chart';

// Components:
import Form from 'Form';
import TransferralBox from 'TransferralBox';
import ListOfObjectsWrapper from 'ListOfObjectsWrapper';
import Paper from 'Paper';

// Helpers:
import state_getters_data from 'state_getters_data';
import global_vars_public from 'global_vars_public';
import do_queries_for_container_or_field from 'do_queries_for_container_or_field';
import url_properties_from_location_or_url_str from 'url_properties_from_location_or_url_str';
import deal_with_incramentally_loaded_entity from 'deal_with_incramentally_loaded_entity';
import get_data from 'get_data';
import get_queries from 'get_queries';
import action_creators from 'action_creators';
import css from 'css';
import transform_form from 'transform_form';
import post_form from 'post_form';

// Forms
import Book from 'Book';

var Container = React.createClass({


    // On future refacturings of this code base this will be done in a more elegant way

    on_click_buy(state, location, _id, isbn, path) {

        return () => {

            if (!location) throw new Error();
            if (!_id) throw new Error();

            var buy_button_form = transform_form(
                Book(),
                'Book_for_buy_button',
                { book_id: _id, isbn: isbn }
            );

            post_form(state, buy_button_form);

            var reload_function = () => {
                do_queries_for_container_or_field(
                    state,
                    false,
                    url_properties_from_location_or_url_str(location),
                    null
                );
            }

            setTimeout(reload_function, 100);
            setTimeout(reload_function, 1000);

        }


    },

    on_click_sell(state, location, _id, isbn, path) {

        return () => {

            if (!location) throw new Error();
            if (!_id) throw new Error();

            var sell_button_form = transform_form(
                Book(),
                'Book_for_sell_button',
                { book_id: _id, isbn: isbn }
            );

            post_form(state, sell_button_form);

            var reload_function = () => {
                do_queries_for_container_or_field(
                    state,
                    false,
                    url_properties_from_location_or_url_str(location),
                    null
                );
            }

            setTimeout(reload_function, 100);
            setTimeout(reload_function, 1000);

        }


    },

    render() {

        var {state, location} = this.props;
        var elements_per_page = 3;

        var url_properties = url_properties_from_location_or_url_str(location);

        var display = url_properties.parameters.display;

        if (!state) throw new Error();
        if (!location) throw new Error();
        if (!url_properties) throw new Error();

        var data = get_data.for_url(state, url_properties);

        var {entities, display_load_more_button} = deal_with_incramentally_loaded_entity(data, url_properties, 'transferral_requests', 200);

        var load_more_button;

        if (display_load_more_button) {

            var load_button_style = {
                fontSize: 16,
                textAlign: 'center',
                width: '100%'
            };

            load_more_button = <LargeFlatButton onClick={this.on_load_more_nutton_click}>More...</LargeFlatButton>

        }

        if (display === 'sellers_only' || display === 'sales_only') {
            entities = entities.filter(x => x.buy_or_sell === 'sell');
        }
        else if (display === 'buyers_only' || display === 'purchases_only') {
            entities = entities.filter(x => x.buy_or_sell === 'buy');
        }

        var rendered_objects_for_wrapper = entities.map(
            entity => <TransferralBox obj={entity} state={state} />
        );

        var keys = entities.map(
            entity => hash_it(entity)
        );

        var style = {
            paddingTop: '60px',
            paddingBottom: '150px',
            marginLeft: 'auto',
            marginRight: 'auto',
            width: '960px'
        };

        var book = data.book;
        var text_above_boxes;

        var s_text_above_boxes = {
            fontSize: '16px',
            width: '100%',
            textAlign: 'center',
            paddingTop: '17px',
            paddingBottom: '10px',
            marginBottom: '20px'
        }

        var number_of_buys = entities.filter(x => x.buy_or_sell === 'buy').length;
        var number_of_sells = entities.filter(x => x.buy_or_sell === 'sell').length;

        var set_to_buy_or_style_text;

        if (book && state.data.user_info) {
            set_to_buy_or_style_text = <div>Click <b style={{ cursor: 'pointer' }} onClick={this.on_click_buy(state, location, book._id, book.isbn, `/${url_properties.pathname}`)}>here</b> if you want to buy this book and <b style={{ cursor: 'pointer' }} onClick={this.on_click_sell(state, location, book._id, book.isbn, `/${url_properties.pathname}`)}>here</b> if you want to sell it.</div>
        }
        else if (book) {
            set_to_buy_or_style_text = <div>Log in to show your own interest in buying or selling.</div>
        }

        if (book) {

            if (display === 'sellers_only') {
                text_above_boxes = (
                    <Paper style={s_text_above_boxes}>
                        <div>
                            Below are the users who want to sell <i>{book.title}</i>. <br />
                            Click <Link to={`/${url_properties.pathname}?display=buyers_only`}> here</Link> to show only buyers. <br />
                            Click <Link to={`/${url_properties.pathname}`}> here</Link> to show both buyers and sellers. <br />
                            {set_to_buy_or_style_text}
                        </div>
                    </Paper>
                );
            }
            else if (display === 'buyers_only') {
                text_above_boxes = (
                    <Paper style={s_text_above_boxes}>
                        <div>
                            Below are the users who want to sell <i>{book.title}</i>. <br />
                            Click <Link to={`/${url_properties.pathname}?display=sellers_only`}> here</Link> to show only sellers. <br />
                            Click <Link to={`/${url_properties.pathname}`}> here</Link> to show both buyers and sellers. <br />
                            {set_to_buy_or_style_text}
                        </div>
                    </Paper>
                );
            }
            else {
                text_above_boxes = (
                    <Paper style={s_text_above_boxes}>
                        <div>
                            Below are the users who either want to buy or sell <i>{book.title}</i>. <br />
                            Click <Link to={`/${url_properties.pathname}?display=buyers_only`}> here</Link> to show only buyers. <br />
                            Click <Link to={`/${url_properties.pathname}?display=sellers_only`}> here</Link> to show only sellers. <br />
                            {set_to_buy_or_style_text}
                        </div>
                    </Paper>
                );
            }

        }
        else if (url_properties.pathname === 'my_books') {

            if (display === 'sales_only') {
                text_above_boxes = (
                    <Paper style={s_text_above_boxes}>
                        <div>
                            Below are books that you want to sell. <br />
                            Click <Link to={`/${url_properties.pathname}?display=purchases_only`}> here</Link> to show only show books you want to buy. <br />
                            Click <Link to={`/${url_properties.pathname}`}> here</Link> to show both books you want to buy and books you want to sell.
                        </div>
                    </Paper>
                );
            }
            else if (display === 'purchases_only') {
                text_above_boxes = (
                    <Paper style={s_text_above_boxes}>
                        <div>
                            Below are books that you want to buy. <br />
                            Click <Link to={`/${url_properties.pathname}?display=sales_only`}> here</Link> to show only books you want to sell. <br />
                            Click <Link to={`/${url_properties.pathname}`}> here</Link> to show both books you want to buy and books you want to sell.
                        </div>
                    </Paper>
                );
            }
            else {
                text_above_boxes = (
                    <Paper style={s_text_above_boxes}>
                        <div style={{ paddingTop: number_of_buys || number_of_sells ? '15px' : null }}>
                            Below are books that you want to buy or sell. <br />
                            Click <Link to={`/${url_properties.pathname}?display=purchases_only`}> here</Link> to show only show books you want to buy. <br />
                            Click <Link to={`/${url_properties.pathname}?display=sales_only`}> here</Link> to show only books you want to sell.
                        </div>
                    </Paper>
                );
            }

        }

        var chart;

        if (entities && entities.length > 0 && display !== 'purchases_only' && display !== 'sales_only') {

            var text;

            if (url_properties.pathname === 'my_books') {
                text = `There are ${number_of_buys} books that you want to buy and ${number_of_sells} that you want to sell.`
            }
            else {
                text = `There are ${number_of_buys} users who want to buy this book and ${number_of_sells} users who want to sell it.`
            }

            var slices = [
                {
                    color: '#FFF0A5',
                    value: number_of_buys,
                },
                {
                    color: '#F8D897',
                    value: number_of_sells,
                }
            ];

            chart = (
                <Paper style={{ padding: '10px', paddingLeft: '25px', paddingRight: '25px', marginBottom: '20px', display: 'flex' }}>
                    <div style={{ width: '45%', marginTop: '10px', marginRight: '20px' }}>
                        <PieChart slices={slices} />
                    </div>
                    <div style={{ flexGrow: 1, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <p>{text}</p>
                    </div>
                </Paper>
            )

        }

        return (
            <div style={style}>
                <div style={{ display: 'flex' }}>
                    {chart}
                    {text_above_boxes}
                </div>
                <ListOfObjectsWrapper
                    objects={entities}
                    rendered_objects={rendered_objects_for_wrapper}
                    component_called_from={'TransferralRequestListContainer'} />
                {load_more_button}
            </div >
        );

    },

    componentDidMount() {

        var {location, state} = this.props;

        if (!location) throw new Error();
        if (!state) throw new Error();

        do_queries_for_container_or_field(
            state,
            false,
            url_properties_from_location_or_url_str(location),
            null
        );

        // Set timeout is used here so that if the action that pointed the user here also added something to the page, the page includes this data also.
        // There are better ways of doing this, and more elegant ways, which would be used instead in a more serious project.
        // One of the ways this code base will be refactured going forward is by making it so that it deals with such issues in a
        // more elegant and abstract way (I have several potential approaches in my mind). If we wanted continuous updates we could use setInterval instead
        // of timeout.

        setTimeout(
            () => do_queries_for_container_or_field(
                state,
                false,
                url_properties_from_location_or_url_str(location),
                null
            ),
            500
        );

        setTimeout(
            () => do_queries_for_container_or_field(
                state,
                false,
                url_properties_from_location_or_url_str(location),
                null
            ),
            1000
        );

    }

});

var mapStateToProps = (state) => {
    return { state: state }
}

var mapDispatchToProps = (dispatch) => {
    return { dispatch: dispatch }
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(Container);