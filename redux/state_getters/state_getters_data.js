import hash_it from 'hash_it';

var ExportsObj = {};

var is_logged_in = (state_data) => {

    var user_info = state_data.user_info;

    if (user_info) return true;
    else return false;

};
ExportsObj.is_logged_in = is_logged_in;

var is_admin = (state_data) => {

    var user_info = state_data.user_info;

    if (user_info) return user_info.is_admin;
    else return false;

};
ExportsObj.is_admin = is_admin;

var get_number_of_times_query_has_been_received = (state_data, query_hash) => {

    if (!state_data) throw new Error('state_data missing');
    if (!query_hash) throw new Error('query_hash missing');

    var number_of_times_query_has_been_received = state_data.number_of_times_queries_have_been_received__by_query_hash[query_hash];

    if (!number_of_times_query_has_been_received) return 0;

    return number_of_times_query_has_been_received;

};
ExportsObj.get_number_of_times_query_has_been_received = get_number_of_times_query_has_been_received;

var get_number_of_times_query_has_been_sent = (state_data, query_hash) => {

    if (!state_data) throw new Error('state_data missing');
    if (!query_hash) throw new Error('query_hash missing');

    var number_of_times_query_has_been_sent = state_data.number_of_times_queries_have_been_sent__by_query_hash[query_hash];

    if (!number_of_times_query_has_been_sent) return 0;

    return number_of_times_query_has_been_sent;

};
ExportsObj.get_number_of_times_query_has_been_sent = get_number_of_times_query_has_been_sent;

var get_query_result = (state_data, query_hash) => {

    if (!state_data) throw new Error('state_data missing');
    if (!query_hash) throw new Error('query_hash missing');

    var result = state_data.query_results__by_query_hash[query_hash];

    if (!result) return null;

    return result;

};
ExportsObj.get_query_result = get_query_result;

var get_last_time_query_was_sent = (state_data, query_hash) => {

    if (!state_data) throw new Error('state_data missing');
    if (!query_hash) throw new Error('query_hash missing');

    var last_time_query_was_sent = state_data.last_time_queries_were_sent__by_query_hash[query_hash];

    if (!last_time_query_was_sent) return null;

    return last_time_query_was_sent;

};
ExportsObj.get_last_time_query_was_sent = get_last_time_query_was_sent;

var get_last_time_query_result_was_received = (state_data, query_hash) => {

    if (!state_data) throw new Error('state_data missing');
    if (!query_hash) throw new Error('query_hash missing');

    var last_time_query_result_was_received = state_data.last_time_query_results_were_received__by_query_hash[query_hash];

    if (!last_time_query_result_was_received) return null;

    return last_time_query_result_was_received;

};
ExportsObj.get_last_time_query_result_was_received = get_last_time_query_result_was_received;


module.exports = ExportsObj;