var ExportsObj = {};

var get_form = (state_form_and_fields, form_key) => {

    if (!state_form_and_fields) throw new Error('state_form_and_fields not provided');

    var form = state_form_and_fields.forms__by_form_key[form_key];

    if (!form) throw new Error(`Could not find form with form_key ${form_key}`);

    return form;

};
ExportsObj.get_form = get_form;

var get_field = (state_form_and_fields, form_key, field_key) => {

    if (!state_form_and_fields) throw new Error('state_form_and_fields not provided');

    var form_obj = state_form_and_fields.forms__by_form_key[form_key];

    var arr_filtered_by_field_key = form_obj.fields.filter(field => field.field_key == field_key);

    if (arr_filtered_by_field_key.length !== 1) throw new Error('Oops..');

    return arr_filtered_by_field_key[0];

};
ExportsObj.get_field = get_field;

var get_option = (state_form_and_fields, form_key, field_key, option_key) => {

    if (!state_form_and_fields) throw new Error('state_form_and_fields not provided');

    var form_obj = state_form_and_fields.forms__by_form_key[form_key];

    var arr_filtered_by_field_key = form_obj.fields.filter(field => field.field_key == field_key);

    if (arr_filtered_by_field_key.length !== 1) throw new Error('Oops..');

    var field = arr_filtered_by_field_key[0];

    var arr_filtered_by_option_key = form.options.filter(option => option.option_key == option_key);

    if (arr_filtered_by_option_key.length !== 1) throw new Error('Oops..');

    return arr_filtered_by_option_key[0];

};
ExportsObj.get_option = get_option;

module.exports = ExportsObj;