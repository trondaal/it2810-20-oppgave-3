import redux_store from 'redux_store';
import { push } from 'react-router-redux';
import cookie_jeep from 'cookie-jeep';


var ExportsObj = {};


var NEW_SEARCH_BAR_VALUE = (new_value, location) => {
    redux_store.dispatch({
        type: 'NEW_SEARCH_BAR_VALUE',
        new_value
    });
}
ExportsObj.NEW_SEARCH_BAR_VALUE = NEW_SEARCH_BAR_VALUE;

var FIRST_RENDER_HAS_BEEN_COMPLETED = () => {
    redux_store.dispatch({
        type: 'FIRST_RENDER_HAS_BEEN_COMPLETED'
    });
}
ExportsObj.FIRST_RENDER_HAS_BEEN_COMPLETED = FIRST_RENDER_HAS_BEEN_COMPLETED;

var UPDATE_TO_LOGGED_IN = (user_info) => {

    if(!cookie_jeep.read('user_info')) {
        cookie_jeep.write('user_info', JSON.stringify(user_info), 1000000);
    }

    redux_store.dispatch({
        type: 'UPDATE_TO_LOGGED_IN',
        user_info: user_info
    });

};
ExportsObj.UPDATE_TO_LOGGED_IN = UPDATE_TO_LOGGED_IN;

var UPDATE_TO_LOGGED_OUT = () => {

    cookie_jeep.delete('user_info');

    redux_store.dispatch({
        type: 'UPDATE_TO_LOGGED_OUT'
    });

};
ExportsObj.UPDATE_TO_LOGGED_OUT = UPDATE_TO_LOGGED_OUT;

var PUSH = (path) => {
    redux_store.dispatch(push(path));

};
ExportsObj.PUSH = PUSH;


var REGISTER_QUERY_AS_SENT = (query, time_sent, location) => {

    if (!query) throw new Error('query missing');
    if (!time_sent) throw new Error('time_sent missing');

    redux_store.dispatch({
        type: 'REGISTER_QUERY_AS_SENT',
        query,
        time_sent,
        location
    });

};
ExportsObj.REGISTER_QUERY_AS_SENT = REGISTER_QUERY_AS_SENT;

var RECEIVE_QUERY = (query, result, time_received) => {

    if (!query) throw new Error('query missing');
    if (!result) throw new Error('time_sent missing');
    if (!time_received) throw new Error('time_sent missing');

    redux_store.dispatch({
        type: 'RECEIVE_QUERY',
        query,
        result,
        time_received
    });

};
ExportsObj.RECEIVE_QUERY = RECEIVE_QUERY;

var REPLACE_FIELD = (old_field, new_field, form_key) => {

    var field_key = old_field.field_key;

    if (!field_key) throw new Error('field_key is missing');

    if (old_field === new_field) throw new Error('Hmm. Did you remember to make a copy of the field before changing it?')
    if (old_field === new_field) throw new Error('Hmm. Did you remember to make a copy of the field before changing it?')

    if (new_field.form_key !== old_field.form_key) throw new Error('Uhm, what?');
    if (new_field.field_key !== old_field.field_key) throw new Error('Uhm, what?');

    redux_store.dispatch({
        type: 'REPLACE_FIELD',
        form_key,
        field_key,
        new_field
    });

};
ExportsObj.REPLACE_FIELD = REPLACE_FIELD;

var CLICK_OPTION = (form_key, field_key, option_key) => {

    if (!form_key) throw new Error('form_key missing');
    if (!field_key) throw new Error('field_key missing');
    if (!option_key) throw new Error('option_key missing');

    redux_store.dispatch({
        type: 'CLICK_OPTION',
        form_key,
        field_key,
        option_key
    });

};
ExportsObj.CLICK_OPTION = CLICK_OPTION;

var CHANGE_KEY_OF_CURRENT_DIALOG = (key, null_current_key) => {

    redux_store.dispatch({
        type: 'CHANGE_KEY_OF_CURRENT_DIALOG',
        key,
        null_current_key
    });

};
ExportsObj.CHANGE_KEY_OF_CURRENT_DIALOG = CHANGE_KEY_OF_CURRENT_DIALOG;

var RESET_FORMS_AND_FIELDS = () => {

    redux_store.dispatch({
        type: 'RESET_FORMS_AND_FIELDS'
    });

};
ExportsObj.RESET_FORMS_AND_FIELDS = RESET_FORMS_AND_FIELDS;


module.exports = ExportsObj;