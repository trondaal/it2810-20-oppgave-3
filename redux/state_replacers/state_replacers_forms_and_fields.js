var ExportsObj = {};


var replace_field = (forms_and_autocompletes_state, field__copied, form_key, field_key) => {

    if (!forms_and_autocompletes_state) throw new Error(`forms_and_autocompletes_state: ${forms_and_autocompletes_state}`);

    var forms_and_autocompletes_state__copied = Object.assign({}, forms_and_autocompletes_state);

    var form__copied = Object.assign({}, forms_and_autocompletes_state__copied.forms__by_form_key[form_key]);

    form__copied.fields.forEach(
        (field, index) => {
            if (field.field_key === field_key) form__copied.fields[index] = field__copied;
        }
    );

    forms_and_autocompletes_state__copied.forms__by_form_key[form_key] = form__copied;

    return Object.assign(
        {},
        forms_and_autocompletes_state__copied
    );

};
ExportsObj.replace_field = replace_field;


var set_key_of_current_dialog = (forms_and_autocompletes_state, form_key_or_null) => {

    if (!forms_and_autocompletes_state) throw new Error(`forms_and_autocompletes_state: ${forms_and_autocompletes_state}`);

    var forms_and_autocompletes_state__copied = Object.assign({}, forms_and_autocompletes_state);

    forms_and_autocompletes_state__copied.key_of_current_dialog = form_key_or_null;

    return Object.assign(
        {},
        forms_and_autocompletes_state__copied
    );

};
ExportsObj.set_key_of_current_dialog = set_key_of_current_dialog;


module.exports = ExportsObj;