import hash_it from 'hash_it';

var ExportsObj = {};

// Temporary:
var initial_state = {

    books__by_id: {},
    categories__by_id: {},

    // query_hash is hashed based on query-object
    query_results__by_query_hash: {},
    last_time_queries_were_sent__by_query_hash: {},
    last_time_query_results_were_received__by_query_hash: {},
    number_of_times_queries_have_been_sent__by_query_hash: {},
    number_of_times_queries_have_been_received__by_query_hash: {}

};

var add_received_query = (state_data, query, result, time_received) => {

    var query_hash = hash_it(query);

    var state_data__copied = Object.assign({}, state_data);

    var query_results__by_query_hash__copied = Object.assign({}, state_data.query_results__by_query_hash);
    var last_time_query_results_were_received__by_query_hash__copied = Object.assign({}, state_data.last_time_query_results_were_received__by_query_hash);
    var number_of_times_queries_have_been_received__by_query_hash__copied = Object.assign({}, state_data.number_of_times_queries_have_been_received__by_query_hash);

    query_results__by_query_hash__copied[query_hash] = result;

    last_time_query_results_were_received__by_query_hash__copied[query_hash] = time_received;
    
    if (number_of_times_queries_have_been_received__by_query_hash__copied[query_hash] === undefined) {
        number_of_times_queries_have_been_received__by_query_hash__copied[query_hash] = 0;
    }
    else {
        number_of_times_queries_have_been_received__by_query_hash__copied[query_hash] += 1;
    }

    state_data__copied.query_results__by_query_hash = query_results__by_query_hash__copied;
    state_data__copied.last_time_query_results_were_received__by_query_hash = last_time_query_results_were_received__by_query_hash__copied;
    state_data__copied.number_of_times_queries_have_been_received__by_query_hash = number_of_times_queries_have_been_received__by_query_hash__copied;

    return state_data__copied;

};
ExportsObj.add_received_query = add_received_query;

var add_sent_query = (state_data, query, time_sent) => {

    var query_hash = hash_it(query);

    var state_data__copied = Object.assign({}, state_data);
    
    var last_time_queries_were_sent__by_query_hash__copied = Object.assign({}, state_data.query_results__by_query_hash);
    var number_of_times_queries_have_been_sent__by_query_hash__copied = Object.assign({}, state_data.query_results__by_query_hash);

    last_time_queries_were_sent__by_query_hash__copied[query_hash] = time_sent;

    if (number_of_times_queries_have_been_sent__by_query_hash__copied[query_hash] === undefined) {
        number_of_times_queries_have_been_sent__by_query_hash__copied[query_hash] = 0;
    }
    else {
        number_of_times_queries_have_been_sent__by_query_hash__copied[query_hash] += 1;
    }

    state_data__copied.last_time_queries_were_sent__by_query_hash = last_time_queries_were_sent__by_query_hash__copied;
    state_data__copied.number_of_times_queries_have_been_sent__by_query_hash = number_of_times_queries_have_been_sent__by_query_hash__copied;

    return state_data__copied;

};
ExportsObj.add_sent_query = add_sent_query;

module.exports = ExportsObj;