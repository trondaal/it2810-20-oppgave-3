// Libraries:
import { compose, createStore, combineReducers, applyMiddleware } from 'redux';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import { browserHistory } from 'react-router';

// Reducers:
import reducer_forms_and_fields from 'reducer_forms_and_fields';
import reducer_data from 'reducer_data';
import reducer_other from 'reducer_other';

var reducers_combined = combineReducers({
    routing: routerReducer,
    forms_and_fields: reducer_forms_and_fields,
    data: reducer_data,
    other: reducer_other
});

var middleware = routerMiddleware(browserHistory);

const redux_store = createStore(
    reducers_combined,
    applyMiddleware(middleware)
);

module.exports = redux_store;