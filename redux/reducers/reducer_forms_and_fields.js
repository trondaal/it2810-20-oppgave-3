// Form-types
import Book from 'Book';
import LogIn from 'LogIn';
import Register from 'Register';

// Helpers
import form_helpers from 'form_helpers';

var initiate_form = form_helpers.initiate_form;
var transform_form = form_helpers.transform_form;

// Reducer-functions
import { REPLACE_FIELD, CLICK_OPTION, CHANGE_KEY_OF_CURRENT_DIALOG, FORM_POST_RESULT, NEW_SEARCH_BAR_VALUE, RESET_FORMS_AND_FIELDS } from 'reducer_functions_forms_and_fields';

var initial_state = {

    forms__by_form_key: {

        Book_sell_book: initiate_form(
            transform_form(Book(), 'Book_sell_book'),
            'Book_sell_book'
        ),
        Book_buy_book: initiate_form(
            transform_form(Book(), 'Book_buy_book'),
        'Book_buy_book'
        ),
        LogIn: initiate_form(LogIn(), 'LogIn'),
        Register: initiate_form(Register(), 'Register')
        
    },

    fields: {},

    search_bar_value: '',

    key_of_current_dialog: null,

    click_count: 0

};


module.exports = (state = initial_state, action) => {

    var { type } = action;

    switch (type) {

        case 'RESET_FORMS_AND_FIELDS':
            return RESET_FORMS_AND_FIELDS(state, action);
        case 'NEW_SEARCH_BAR_VALUE':
            return NEW_SEARCH_BAR_VALUE(state, action);
        case 'FORM_POST_RESULT':
            return FORM_POST_RESULT(state, action);
        case 'REPLACE_FIELD':
            return REPLACE_FIELD(state, action);
        case 'CLICK_OPTION':
            return CLICK_OPTION(state, action);
        case 'CHANGE_KEY_OF_CURRENT_DIALOG':
            return CHANGE_KEY_OF_CURRENT_DIALOG(state, action);
        default:
            return state;

    }

};