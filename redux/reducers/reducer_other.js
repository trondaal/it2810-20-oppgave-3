var initial_state = {

    first_render_has_been_completed: false,

};


module.exports = (state = initial_state, action) => {

    var { type } = action;

    switch (type) {

        case 'FIRST_RENDER_HAS_BEEN_COMPLETED':
            return {
                first_render_has_been_completed: true
            }
        default:
            return state;

    }

};