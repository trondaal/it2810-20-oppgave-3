// Reducer-functions
import {REGISTER_QUERY_AS_SENT, RECEIVE_QUERY, UPDATE_TO_LOGGED_IN, UPDATE_TO_LOGGED_OUT, UPDATE_LOCATION} from 'reducer_functions_data';

var initial_state = {

    // Not currently in use, but could help reduce the number of neccecary queries
    books__by_id: {},
    categories__by_id: {},

    // Keeps track of the URL that is visited
    current_location: null,
    time_of_last_location_change: null,
    last_time_queries_were_sent_for_location__by_location_hash: {},

    user_info: null,

    // query_hash is hashed based on query-object
    query_results__by_query_hash: {},
    last_time_queries_were_sent__by_query_hash: {},
    last_time_query_results_were_received__by_query_hash: {},
    number_of_times_queries_have_been_sent__by_query_hash: {},
    number_of_times_queries_have_been_received__by_query_hash: {}

};


module.exports = (state = initial_state, action) => {

    var { type } = action;

    switch (type) {
        
        case 'REGISTER_QUERY_AS_SENT':
            return REGISTER_QUERY_AS_SENT(state, action);
        case 'RECEIVE_QUERY':
            return RECEIVE_QUERY(state, action);
        case 'UPDATE_TO_LOGGED_IN':
            return UPDATE_TO_LOGGED_IN(state, action);
        case 'UPDATE_TO_LOGGED_OUT':
            return UPDATE_TO_LOGGED_OUT(state, action);
        default:
            return state;

    }

};