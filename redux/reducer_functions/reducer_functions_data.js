// Libraries
import hash_it from 'hash_it';

import state_getters from 'state_getters_data';
import state_replacers from 'state_replacers_data';

import global_vars_public from 'global_vars_public';

import action_creators from 'action_creators';

var API_ROOT = global_vars_public.API_ROOT;

var ExportsObj = {};

var UPDATE_TO_LOGGED_IN = (state_data, {user_info}) => {

    if (!user_info) throw new Error();

    var state_data_copied = Object.assign({}, state_data);

    state_data_copied.user_info = user_info;

    return state_data_copied;

};
ExportsObj.UPDATE_TO_LOGGED_IN = UPDATE_TO_LOGGED_IN;

var UPDATE_TO_LOGGED_OUT = (state_data, action) => {

    var state_data_copied = Object.assign({}, state_data);

    state_data_copied.user_info = null;

    return state_data_copied;

};
ExportsObj.UPDATE_TO_LOGGED_OUT = UPDATE_TO_LOGGED_OUT;

var REGISTER_QUERY_AS_SENT = (state_data, {query, time_sent, location}) => {

    var query_hash = hash_it(query);

    var {query_type, entity_type} = query;

    if (!query_type) throw new Error('query missing');
    if (!entity_type) throw new Error('entity_type missing');
    if (!time_sent) throw new Error('time_sent missing');

    if (query_type === 'retrieve' || query_type === 'find_one_entity') {
        if (entity_type === undefined) throw new Error('entity_type missing');
    }

    // if (location) {

    //     var new_location_hashed = hash_it(location);
    //     var old_location_hashed = hash_it(state_data.current_location);

    //     if (new_location_hashed !== old_location_hashed) {
    //         throw new Error();
    //     }

    //     var state_data_copied = Object.assign({}, state_data);

    //     state_data_copied.queries_have_been_sent_for_current_location = true;

    // }

    return state_replacers.add_sent_query(state_data, query, time_sent);

};
ExportsObj.REGISTER_QUERY_AS_SENT = REGISTER_QUERY_AS_SENT;

var RECEIVE_QUERY = (state_data, action) => {

    var {query, result, time_received} = action;

    if (!query) throw new Error('query missing');
    if (!result) throw new Error('result missing');
    if (!time_received) throw new Error('time_received missing');

    return state_replacers.add_received_query(state_data, query, result, time_received);

};
ExportsObj.RECEIVE_QUERY = RECEIVE_QUERY;

module.exports = ExportsObj;