import state_getters from 'state_getters_forms_and_fields';
import state_replacers from 'state_replacers_forms_and_fields';


// Form-types
import Book from 'Book';
import LogIn from 'LogIn';
import Register from 'Register';

import form_helpers from 'form_helpers';
var initiate_form = form_helpers.initiate_form;
var transform_form = form_helpers.transform_form;

var ExportsObj = {};

var CHANGE_KEY_OF_CURRENT_DIALOG = (forms_and_autocompletes_state, {key, null_current_key}) => {

    if (null_current_key && key) throw new Error();
    if (!null_current_key && !key) throw new Error();

    if (key && key !== 'log in or register' && !state_getters.get_form(forms_and_autocompletes_state, key)) {
        throw new Error('There is no form with this key');
    }

    if (null_current_key) return state_replacers.set_key_of_current_dialog(forms_and_autocompletes_state, null);
    else return state_replacers.set_key_of_current_dialog(forms_and_autocompletes_state, key);

};
ExportsObj.CHANGE_KEY_OF_CURRENT_DIALOG = CHANGE_KEY_OF_CURRENT_DIALOG;

var REPLACE_FIELD = (forms_and_autocompletes_state, { form_key, field_key, new_field }) => {

    if (form_key === undefined) throw new Error('form_key missing');
    if (field_key === undefined) throw new Error('field_key missing');
    if (new_field === undefined) throw new Error('new_field missing');

    return state_replacers.replace_field(
        forms_and_autocompletes_state,
        new_field,
        form_key,
        field_key
    );

};
ExportsObj.REPLACE_FIELD = REPLACE_FIELD;

var NEW_SEARCH_BAR_VALUE = (forms_and_autocompletes_state, { new_value }) => {

    var forms_and_autocompletes_state_copied = Object.assign({}, forms_and_autocompletes_state);
    if (!new_value) new_value = '';
    forms_and_autocompletes_state_copied.search_bar_value = new_value;

    return forms_and_autocompletes_state_copied;

};
ExportsObj.NEW_SEARCH_BAR_VALUE = NEW_SEARCH_BAR_VALUE;

var RESET_FORMS_AND_FIELDS = (forms_and_autocompletes_state) => {

    var forms_and_autocompletes_state_copied = Object.assign({}, forms_and_autocompletes_state);

    var forms__by_form_key_copied = Object.assign({}, forms_and_autocompletes_state.forms__by_form_key);

    forms__by_form_key_copied.Book_sell_book = initiate_form(
        transform_form(Book(), 'Book_sell_book'),
        'Book_sell_book'
    );

    forms__by_form_key_copied.Book_buy_book = initiate_form(
        transform_form(Book(), 'Book_buy_book'),
        'Book_buy_book'
    );

    forms__by_form_key_copied.LogIn = initiate_form(LogIn(), 'LogIn');

    forms__by_form_key_copied.Register = initiate_form(Register(), 'Register');

    forms_and_autocompletes_state_copied.forms__by_form_key = forms__by_form_key_copied;

    return forms_and_autocompletes_state_copied;

};
ExportsObj.RESET_FORMS_AND_FIELDS = RESET_FORMS_AND_FIELDS;



module.exports = ExportsObj;

