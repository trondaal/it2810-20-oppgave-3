var hover_div = {
    backgroundColor: this.is_being_hovered() ? s('backgroundColor', 'BookBox', 'hover_div') : 'rgba(0, 0, 0, 0)',
    height: `${hover_div_height}px`,
    marginTop: `-${hover_div_height}px`,
    borderRadius: s('borderRadius', 'BookBox'),
    cursor: 'pointer',
    position: 'relative'
}