// Based on: https://gist.github.com/learncodeacademy/25092d8f1daf5e4a6fd3, https://www.youtube.com/watch?v=9kJVYpOqcVU

var webpack = require('webpack');
var resolve = require('./webpack_configs_resolve');

module.exports = {
    context: __dirname,
    entry: "./client_setup/client.js",
    output: {
        path: __dirname + "/public/",
        filename: "bundle_prod.js"
    },
    plugins: [
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurenceOrderPlugin(),
        // new webpack.optimize.UglifyJsPlugin({
        //     mangle: false,
        //     sourcemap: false }
        // ),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
        })
    ],
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['react', 'es2015']
                }
            }
        ]
    },
    resolve: resolve
};