var path = require('path');

module.exports = {
    alias: {

        // Components, abstract
        Paper: path.resolve('./components/general/Paper.jsx'),
        // Components, boxes
        BookBox: path.resolve('./components/boxes/BookBox.jsx'),
        TransferralBox: path.resolve('./components/boxes/TransferralBox.jsx'),
        // Components, buttons
        ActionButton: path.resolve('./components/buttons/ActionButton.jsx'),
        FlatButton: path.resolve('./components/buttons/FlatButton.jsx'),
        LargeFlatButton: path.resolve('./components/buttons/LargeFlatButton.jsx'),
        // Components, form
        Form: path.resolve('./components/form/Form.jsx'),
        FormField: path.resolve('./components/form/FormField.jsx'),
        // Components, form, field_types
        Autocomplete: path.resolve('./components/form/field_types/Autocomplete.jsx'),
        Options: path.resolve('./components/form/field_types/Options.jsx'),
        TextField: path.resolve('./components/form/field_types/TextField.jsx'),
        // Components, navbar
        Navbar: path.resolve('./components/navbar/Navbar.jsx'),
        // Components, wrappers
        ListOfObjectsWrapper: path.resolve('./components/wrappers/ListOfObjectsWrapper.jsx'),
        NonPopupFormWrapper: path.resolve('./components/wrappers/NonPopupFormWrapper.jsx'),

        // Container_components
        BookContainer: path.resolve('./container_components/BookContainer.jsx'),
        BookListContainer: path.resolve('./container_components/BookListContainer.jsx'),
        BookPageContainer: path.resolve('./container_components/BookPageContainer.jsx'),
        FrontPageContainer: path.resolve('./container_components/FrontPageContainer.jsx'),
        MainContainer: path.resolve('./container_components/MainContainer.jsx'),
        TransferralRequestListContainer: path.resolve('./container_components/TransferralRequestListContainer.jsx'),

        // Form_types
        Book: path.resolve('./form_types/Book.js'),
        LogIn: path.resolve('./form_types/LogIn.js'),
        Register: path.resolve('./form_types/Register.js'),

        // Helpers
        form_handling_helpers: path.resolve('./helpers/form_handling_helpers.js'),
        form_helpers: path.resolve('./helpers/form_helpers.js'),
        helpers_from_stack_overflow: path.resolve('./helpers/helpers_from_stack_overflow.js'),
        key_helpers: path.resolve('./helpers/key_helpers.js'),
        sanitizer_helpers: path.resolve('./helpers/sanitizer_helpers.js'),
        style_helpers: path.resolve('./helpers/style_helpers.js'),
        string_helpers: path.resolve('./helpers/string_helpers.js'),
        array_helpers: path.resolve('./helpers/array_helpers.js'),
        object_helpers: path.resolve('./helpers/object_helpers.js'),
        query_and_data_fetchers_helper: path.resolve('./helpers/query_and_data_fetchers_helper.js'),
        // Helpers, individual_functions
        deal_with_incramentally_loaded_entity: path.resolve('./helpers/individual_functions/deal_with_incramentally_loaded_entity.js'),
        url_properties_from_location_or_url_str: path.resolve('./helpers/individual_functions/url_properties_from_location_or_url_str.js'),
        css: path.resolve('./helpers/individual_functions/css.js'),
        process_user_input_based_html_for_react: path.resolve('./helpers/individual_functions/process_user_input_based_html_for_react.js'),
        split_into_Ns: path.resolve('./helpers/individual_functions/split_into_Ns.js'),
        blend_colors_by_percentage: path.resolve('./helpers/individual_functions/blend_colors_by_percentage.js'),
        darken_color_by_percentage: path.resolve('./helpers/individual_functions/darken_color_by_percentage.js'),
        lighten_color_by_percentage: path.resolve('./helpers/individual_functions/lighten_color_by_percentage.js'),
        deduce_width_from_paddings: path.resolve('./helpers/individual_functions/deduce_width_from_paddings.js'),
        numberize_style_var: path.resolve('./helpers/individual_functions/numberize_style_var.js'),
        hash_it: path.resolve('./helpers/individual_functions/hash_it.js'),
        transform_form: path.resolve('./helpers/individual_functions/transform_form.js'),
        // Redux
        redux_store: path.resolve('./redux/redux_store.js'),
        action_creators: path.resolve('./redux/action_creators.js'),
        // Redux, reducer_functions
        reducer_functions_forms_and_fields: path.resolve('./redux/reducer_functions/reducer_functions_forms_and_fields.js'),
        reducer_functions_data: path.resolve('./redux/reducer_functions/reducer_functions_data.js'),
        // Redux, reducers
        reducer_forms_and_fields: path.resolve('./redux/reducers/reducer_forms_and_fields.js'),
        reducer_data: path.resolve('./redux/reducers/reducer_data.js'),
        reducer_other: path.resolve('./redux/reducers/reducer_other.js'),
        // Redux, state_getters
        state_getters_forms_and_fields: path.resolve('./redux/state_getters/state_getters_forms_and_fields.js'),
        state_getters_data: path.resolve('./redux/state_getters/state_getters_data.js'),
        // Redux, state_replacers
        state_replacers_forms_and_fields: path.resolve('./redux/state_replacers/state_replacers_forms_and_fields.js'),
        state_replacers_data: path.resolve('./redux/state_replacers/state_replacers_data.js'),

        // Queries
        get_queries: path.resolve('./queries/get_queries.js'),
        get_data: path.resolve('./queries/get_data.js'),
        // Queries, query_senders
        do_queries_for_container_or_field: path.resolve('./queries/query_senders/do_queries_for_container_or_field.js'),
        do_get_query: path.resolve('./queries/query_senders/do_get_query.js'),
        post_form: path.resolve('./queries/query_senders/post_form.js'),
        push_button: path.resolve('./queries/query_senders/push_button.js'),
        // Queries, query_senders, helpers
        fetch_headers: path.resolve('./queries/query_senders/helpers/fetch_headers.js'),

        // Other
        style_vars: path.resolve('./styling/style_vars.js'),
        global_vars_public: path.resolve('./global_vars_public.js')

    }
}