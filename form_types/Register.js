module.exports = () => {

    return {
        entity_type: 'User',
        form_type: 'Register',
        post_button_text: 'Register',
        fields: [
            {
                text: 'E-mail',
                value: '',
                field_type: 'textfield',
                pre_change_value: '',
                field_name: 'email'
            },
            {
                text: 'Username',
                value: '',
                field_type: 'textfield',
                pre_change_value: '',
                field_name: 'username'
            },
            {
                text: 'URL of profile image',
                value: '',
                field_type: 'textfield',
                pre_change_value: '',
                field_name: 'image'
            },
            {
                text: 'Password',
                value: '',
                field_type: 'textfield',
                pre_change_value: '',
                field_name: 'password',
                is_password_field: true
            }
        ]
    }

};