module.exports = () => {

    return {

        entity_type: 'User',
        form_type: 'LogIn',
        post_button_text: 'Log in',
        fields: [
            {
                text: 'Username',
                value: '',
                field_type: 'textfield',
                pre_change_value: '',
                field_name: 'username'
            },
            {
                text: 'Password',
                value: '',
                field_type: 'textfield',
                pre_change_value: '',
                field_name: 'password',
                is_password_field: true,
            }
        ]

    }

};