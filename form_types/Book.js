module.exports = () => {
    return {

        form_type: 'Book',
        post_button_text: 'Add book',
        fields: [
            {
                text: 'What is the title of the book?',
                value: '',
                field_type: 'textfield',
                pre_change_value: '',
                field_name: 'title'
            },
            {
                text: 'What is the ISBN-number of the book?',
                value: '',
                field_type: 'textfield',
                pre_change_value: '',
                field_name: 'isbn'
            },
            {
                text: 'What is an URL of a picture of the book?',
                value: '',
                field_type: 'textfield',
                pre_change_value: '',
                field_name: 'image'
            },
            {
                text: 'Do you want to buy or sell?',
                field_type: 'options',
                max_number_of_selections_allowed: 1,
                min_number_of_selections_allowed: 1,
                value: null,
                options: [
                    {
                        text: 'Buy',
                        is_input_field: false,
                        is_selected: false,
                        value: 'buy'
                    },
                    {
                        text: 'Sell',
                        is_input_field: false,
                        is_selected: false,
                        value: 'sell'
                    }
                ],
                field_name: 'buy_or_sell'
            },
            {
                dont_display: true,
                field_name: 'book_id'
            }
        ]
    }

};
