var path = require('path');
var webpack = require('webpack');

var resolve = require('./webpack_configs_resolve');

module.exports = {
    context: __dirname,
    //devtool: 'eval',
    devtool: "inline-sourcemap",
    entry: [
        'webpack-hot-middleware/client',
        './client_setup/client'
    ],
    output: {
        path: `${__dirname}/public'`,
        filename: 'bundle_dev.js',
        publicPath: '/'
    },
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin()
    ],
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['react', 'es2015', 'react-hmre']
                }
            }
        ]
    },
    resolve: resolve
};