// Initiate mongoose if not already initiated:
var mongoose = require('mongoose');
if (mongoose.connection.readyState == 0) {
    mongoose.connect('mongodb://localhost:27017/test');
    mongoose.Promise = require('bluebird');
}

var models_and_model_settings = require('../models/models_and_model_settings');

models_and_model_settings.model_names.forEach(
    entity_type_name => {

        var Entity_model = models_and_model_settings[entity_type_name];

        Entity_model.remove().exec(
            (error, result) => {
                if (error) {
                    console.log('error', error);
                }
                else {
                    console.log(`Entities of type ${entity_type_name} were successfully removed it seems`);
                }
            }
        );

    }
);