// Initiate mongoose if not already initiated:
var mongoose = require('mongoose');
if (mongoose.connection.readyState == 0) {
    mongoose.connect('mongodb://localhost:27017/test');
    mongoose.Promise = require('bluebird');
}

// Libraries
var request = require('request');
var xml_string_to_js_object = require('xml2js').parseString;

var autocomplete_field = require('../queries/query_receivers/helpers/autocomplete_field.js');
var models_and_model_settings = require('../models/models_and_model_settings.js');
Book_model = models_and_model_settings['Book'];



var query_result = null;
var query_result_was_fetched_from_DB = null;
var query_result_was_fetched_from_API = null;


var make_API_query = (name, isbn) => {
    return new Promise(
        (resolve, reject) => {

            query_result = [];
            // var url = `http://partner.bokkilden.no/SamboWeb/partner.do?rom=MP&format=XML&uttrekk=5&pid=0&ept=3&xslId=117&antall=20&&profil=partner&order=DESC&enkeltsok=${name}`;
            var url = `http://partner.bokkilden.no/SamboWeb/partner.do?rom=MP&format=XML&uttrekk=5&pid=0&ept=3&xslId=117&antall=20&&profil=partner&order=DESC&enkeltsok=${name}`;

            request(url, (error, response, body) => {

                console.log('---------------------------------');

                xml_string_to_js_object(body, (err, result) => {

                    console.log(result);

                    books = [];

                    if (result.Produkter && result.Produkter.Produkt) {

                        var index = 0;

                        for (var bok of result.Produkter.Produkt) {

                            index++;
                            console.log(index);

                            var book = new Book_model();

                            var title = bok.Tittel[0];
                            book.title = `${title.charAt(0).toUpperCase()}${title.substring(1)}`;
                            book.title__autocomplete = autocomplete_field(title, true);
                            book.isbn = bok.ISBN[0];
                            book.description = bok.Ingress[0];
                            book.read_more_url = bok.lesmerURL[0];
                            book.image = `http://www.bokkilden.no/SamboWeb/servlet/VisBildeServlet?produktId=${book.read_more_url.substring(62, 69)}&width=400`;
                            if (book.isbn === isbn) {
                                books.push(book)
                            }

                        }

                        resolve(books)

                    }
                    else {
                        resolve(books);
                    }



                });

            });

        }
    )
};

var save_book_to_database = (books) => {

    return new Promise(
        (resolve, reject) => {

            books.forEach(
                book => book.save(
                    (error, result) => {
                        console.log(error);
                        resolve();
                    }
                )
            )

        }
    )

};

var books = [
    {
        name: 'Kommunikasjon i relasjoner samhandling, konfliktløsning, etikk',
        isbn: '9788205326897'
    },
    {
        name: 'Menneskekroppen: fysiologi og anatomi',
        isbn: '9788205348080'
    },
    {
        name: 'Metode- og oppgaveskriving for studenter',
        isbn: '9788205423985'
    },
    {
        name: 'Kommunikasjon i relasjoner: samhandling, konfliktløsning, etikk',
        isbn: '9788205326897'
    },
    {
        name: 'Hvordan organisasjoner fungerer',
        isbn: '9788245014457'
    },
    {
        name: 'Velferdsstaten i endring: norsk sosialpolitikk ved starten av et nytt århundre',
        isbn: '9788205393608'
    },
    {
        name: 'Gode fagfolk vokser: personlig kompetanse i arbeid med mennesker',
        isbn: '9788202350147'
    },
    {
        name: 'Introduksjon til samfunnsvitenskapelig metode',
        isbn: '9788279352983'
    },
    {
        name: 'Enhet og mangfold: samfunnsvitenskapelig forskning og kvantitativ metode',
        isbn: '9788245013283'
    },
    {
        name: 'Objective-C Programming: The Big Nerd Ranch Guide',
        isbn: '9780321706287'
    },
    {
        name: 'Heinemann Modular Maths Edexcel Further Pure Maths 2',
        isbn: '9780435511012'
    },
    //  
];

books.forEach(
    (book, index) => {
        setTimeout(
            () => make_API_query(book.name, book.isbn).then(save_book_to_database).catch((error) => console.log(error)),
            2000*index
        )
    }
);


