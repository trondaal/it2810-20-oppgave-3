// Not currently in use


var models_and_model_settings = require('../models/models_and_model_settings.js');
var sha1 = require('sha1');
var password_hash_key = require('../global_vars_private.js').password_hash_key;


// Initiate mongoose if not already initiated:
var mongoose = require('mongoose');
if (mongoose.connection.readyState == 0) {
    mongoose.connect('mongodb://localhost:27017/test');
    mongoose.Promise = require('bluebird');
}


var mongoose = require('mongoose');


var obj_container_conainer = {};


var save_entity = (model, index) => {
    return new Promise(
        (resolve, reject) => {

            var Model = models_and_model_settings[model];
            var obj_container = obj_container_conainer[model];

            if (!Model) throw new Error('if (!Model)');

            var entity = Model();
            Object.assign(entity, obj_container[index]);

            entity.save(
                (error, result) => {
                    if (error) {
                        console.log(`Error in creating ${model}`);
                        reject(error);
                    }
                    else {
                        obj_container[index] = result;
                        console.log(`Success in creating ${model}`);
                        resolve();
                    }
                }
            );


        }
    )
};




// Users

obj_container_conainer['User'] = {};

obj_container_conainer['User'][1] = {
    username: 'bob',
    email: 'bob@bob.bob',
    image: 'http://www.themarq.ca/images/_theme/student-homepage2.png',
    hashed_password: sha1(`bob${password_hash_key}`),
    is_admin: true
};

obj_container_conainer['User'][2] = {
    username: 'john',
    email: 'not_a_real_email@totallyfake.com',
    image: 'http://img2.rnkr-static.com/list_img_v2/13713/353713/C520/the-very-best-of-the-college-freshman-meme-u1.jpg',
    hashed_password: sha1(`123${password_hash_key}`)
};

obj_container_conainer['User'][3] = {
    username: 'bob',
    email: 'bob@bob.bob',
    image: 'http://img2.rnkr-static.com/list_img_v2/13713/353713/C520/the-very-best-of-the-college-freshman-meme-u1.jpg',
    hashed_password: sha1(`123${password_hash_key}`)
};

obj_container_conainer['User'][4] = {
    username: 'lisa',
    email: 'lisa@gmail.com',
    image: 'https://thumbs.dreamstime.com/t/teen-girl-lot-books-20728061.jpg',
    hashed_password: sha1(`secret${password_hash_key}`)
};

obj_container_conainer['User'][5] = {
    username: 'harald',
    email: 'harald@gmail.com',
    image: 'http://i.imgur.com/In1gzus.png',
    hashed_password: sha1(`harald!${password_hash_key}`)
};






Promise.all([
    save_entity('User', 1),
    save_entity('User', 2),
    save_entity('User', 3),
    save_entity('User', 4),
    save_entity('User', 5)
])
    .then(
    () => {
        console.log('That worked it seems');
        console.log(obj_container_conainer);
    }
    );

