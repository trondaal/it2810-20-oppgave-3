// module.exports = {
//     API_ROOT: `http://it2810-20.idi.ntnu.no/api`,
//     origin: `http://it2810-20.idi.ntnu.no`,
//     default_profile_image: 'https://www.cvrc.org/wp-content/uploads/2016/05/index-1.png',
//     IS_DEV: false,
//     IS_PROD: true,
//     PORT: 80
// }

var PORT = 8080;

module.exports = {
    API_ROOT: `http://localhost:${PORT}/api`,
    origin: `http://localhost:${PORT}`,
    PORT: PORT,
    IS_DEV: true,
    IS_PROD: false,
    default_profile_image: 'https://www.cvrc.org/wp-content/uploads/2016/05/index-1.png'
}