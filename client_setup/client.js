// Libraries:
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router';
import { syncHistory, routeReducer } from 'react-router-redux';
import { compose, createStore, combineReducers, applyMiddleware } from 'redux';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';
import { Client } from 'node-rest-client';

// Containers:
import BookListContainer from 'BookListContainer';
import TransferralRequestListContainer from 'TransferralRequestListContainer';
import MainContainer from 'MainContainer';

//Components
import Navbar from 'Navbar';

// Other
import s from 'style_vars';

import redux_store from 'redux_store'

var history = syncHistoryWithStore(browserHistory, redux_store);

var router = (

    <Provider store={redux_store}>
        <Router history={history}>
            <Route path="/" component={MainContainer}>
                <IndexRoute component={BookListContainer} />
                <Route path="/book/:id" component={TransferralRequestListContainer} />
                <Route path="/my_books" component={TransferralRequestListContainer} />
            </Route>
        </Router>
    </Provider>

);

ReactDOM.render(router, document.getElementById('render-target'));