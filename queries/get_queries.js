import http from 'http';
import hash_it from 'hash_it';

import string_helpers from 'string_helpers';
import object_helpers from 'object_helpers';
import query_and_data_fetchers_helper from 'query_and_data_fetchers_helper';

import state_getters_data from 'state_getters_data';

import global_vars_public from 'global_vars_public';

import get_data from 'get_data';


var ExportsObj = {};

var API_ROOT = global_vars_public.API_ROOT;


var query_should_be_sent = (state, query) => {

    var query_hash = hash_it(query);
    var query_result = state_getters_data.get_query_result(state.data, query_hash);
    var number_of_times_query_has_been_sent = state_getters_data.get_number_of_times_query_has_been_sent(state.data, query_hash);
    var number_of_times_query_has_been_received = state_getters_data.get_number_of_times_query_has_been_received(state.data, query_hash);
    var last_time_query_was_sent = state_getters_data.get_last_time_query_was_sent(state.data, query_hash);
    var last_time_query_result_was_received = state_getters_data.get_last_time_query_result_was_received(state.data, query_hash);

    // TODO: Improve on this logic:

    if (number_of_times_query_has_been_sent === 0) {
        return true;
    }

    var now = new Date();

    // This *temporary* and simplistic choice is pretty though on the database:
    if (now - last_time_query_result_was_received > 200) {
        return true;
    }
    else {
        return false;
    }

};


var for_url = (state, url_properties_or_url_str) => {

    var search_bar_value = state.forms_and_fields.search_bar_value;
    if (search_bar_value === '') search_bar_value = null;

    var pathname_arr, parameters, hash, pathname;

    if (typeof url_properties_or_url_str === 'string') {

        var url_properties = query_and_data_fetchers_helper.url_properties_from_location_or_url_str(url_properties_or_url_str);

        pathname_arr = url_properties.pathname_arr;
        pathname_arr = parameters.pathname_arr;
        pathname_arr = hash.pathname_arr;
        pathname_arr = pathname.pathname_arr;

    }
    else {

        pathname_arr = url_properties_or_url_str.pathname_arr;
        parameters = url_properties_or_url_str.parameters;
        hash = url_properties_or_url_str.hash;
        pathname = url_properties_or_url_str.pathname;

    }

    var queries = [];

    if (pathname === '/' || pathname === '') {

        var elements_per_page = 9;
        var page = parameters.page;
        if (!page) page = 1;
        page = parseInt(page, 10);

        for (var i = 1; i <= page; i++) {

            queries.push({
                query_type: 'retrieve',
                entity_type: 'Book',
                find_several_entities: true,
                parameters: {
                    title__autocomplete: search_bar_value
                },
                limit: elements_per_page,
                skip: (i - 1) * elements_per_page,
                key_for_storage_of_result: `books_page_${i}`
            });

        }

        queries.push({
            query_type: 'retrieve',
            entity_type: 'Book',
            count: true,
            parameters: {
                title__autocomplete: search_bar_value
            },
            key_for_storage_of_result: `books_count`
        });

    }
    else if (pathname_arr[0] && pathname_arr[0] === 'book') {

        queries.push({
            query_type: 'retrieve',
            entity_type: 'Book',
            find_one_entity: true,
            parameters: {
                book_slug: pathname_arr[1] 
            },
            key_for_storage_of_result: 'book'
        });

        var elements_per_page = 200;
        var page = parameters.page;
        if (!page) page = 1;
        page = parseInt(page, 10);

        for (var i = 1; i <= page + 1; i++) {

            queries.push({
                query_type: 'retrieve',
                entity_type: 'Transferral_request',
                find_several_entities: true,
                parameters: {
                    book_slug: pathname_arr[1]
                },
                limit: elements_per_page,
                skip: (i - 1) * elements_per_page,
                key_for_storage_of_result: `transferral_requests_page_${i}`
            });

        }

        queries.push({
            query_type: 'retrieve',
            entity_type: 'Transferral_request',
            count: true,
            parameters: {
                book_slug: pathname_arr[1]
            },
            key_for_storage_of_result: `transferral_requests_count`
        });

    }
    else if (pathname_arr[0] && pathname_arr[0] === 'my_books') {

        var elements_per_page = 200;
        var page = parameters.page;
        if (!page) page = 1;
        page = parseInt(page, 10);

        for (var i = 1; i <= page + 1; i++) {

            queries.push({
                query_type: 'retrieve',
                entity_type: 'Transferral_request',
                find_several_entities: true,
                parameters: {
                    user_ref: state.data.user_info._id
                },
                limit: elements_per_page,
                skip: (i - 1) * elements_per_page,
                key_for_storage_of_result: `transferral_requests_page_${i}`
            });

        }

        queries.push({
            query_type: 'retrieve',
            entity_type: 'Transferral_request',
            count: true,
            parameters: {
                user_ref: state.data.user_info._id
            },
            key_for_storage_of_result: `transferral_requests_count`
        });

    }
    else {

        throw new Error('URL not accounted for in queries_and_data_for_container_or_field.js');

    }

    return queries;

};
ExportsObj.for_url = for_url;


var that_should_be_sent_for_url = (state, url_properties_or_url_str) => {

    return for_url(state, url_properties_or_url_str).filter(
        query => query_should_be_sent(state, query)
    );

};
ExportsObj.that_should_be_sent_for_url = that_should_be_sent_for_url;


var for_field = (state, field) => {

    if (!state) throw new Error();
    if (!field) throw new Error();

    if (!field.queries) return [];

    return field.queries.map(x => x.query);

};
ExportsObj.for_field = for_field;

var that_should_be_sent_for_field = (state, field) => {

    if (!state) throw new Error();
    if (!field) throw new Error();

    return for_field(state, field).filter(
        query => query_should_be_sent(state, query)
    );

};
ExportsObj.that_should_be_sent_for_field = that_should_be_sent_for_field;


var for_main_container = (state) => {

    if (!state) throw new Error();

    return [];

};
ExportsObj.for_main_container = for_main_container;

var that_should_be_sent_for_main_container = (state) => {

    if (!state) throw new Error();

    return for_main_container(state).filter(
        query => query_should_be_sent(state, query)
    );

};
ExportsObj.that_should_be_sent_for_main_container = that_should_be_sent_for_main_container;


module.exports = ExportsObj;