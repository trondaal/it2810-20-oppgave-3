import http from 'http';
import hash_it from 'hash_it';

import string_helpers from 'string_helpers';

import state_getters_data from 'state_getters_data';

import global_vars_public from 'global_vars_public';

import get_queries from 'get_queries';
import query_and_data_fetchers_helper from 'query_and_data_fetchers_helper';

var ExportsObj = {};

var API_ROOT = global_vars_public.API_ROOT;


var decide_key_for_query_result = (query) => {

    if (query.key_for_storage_of_result) {
        return query.key_for_storage_of_result
    }
    else {
        throw new Error('key_for_storage_of_result of query not specified')
    }

};

var fetch_query_result = (state, query) => {

    var query_hash = hash_it(query);

    var query_result = state_getters_data.get_query_result(state.data, query_hash);

    return query_result;

};

var for_url = (state, url_properties) => {

    if (!state) throw new Error();
    if (!url_properties) throw new Error();

    var return_obj = {};

    var queries = get_queries.for_url(state, url_properties);

    for (var query of queries) {
        return_obj[decide_key_for_query_result(query)] = fetch_query_result(state, query);
    }

    return return_obj;

};
ExportsObj.for_url = for_url;

var for_main_container = (state) => {

    //

};
ExportsObj.for_main_container = for_main_container;

var for_field = (state, field) => {

    var return_obj = {};

    var queries = get_queries.for_field(state, field);

    for (var query of queries) {
        return_obj[decide_key_for_query_result(query)] = fetch_query_result(state, query);
    }

    return return_obj;

};
ExportsObj.for_field = for_field;


module.exports = ExportsObj;