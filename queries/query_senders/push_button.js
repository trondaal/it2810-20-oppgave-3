// Libraries
import { Client } from 'node-rest-client';

import action_creators from 'action_creators';

import fetch_headers from 'fetch_headers';

import global_vars_public from 'global_vars_public';

var client = new Client();

module.exports = (state, {button_type, entity_type, entity_id}) => {

    console.log('..state', state)

    if (!state) throw new Error();
    if (!button_type) throw new Error();
    if (!entity_type) throw new Error();
    if (!entity_id) throw new Error();

    var URL = `${global_vars_public.API_ROOT}/button_pushed?button_type=${button_type}`;

    URL += `&entity_type=${entity_type}`;
    URL += `&entity_id=${entity_id}`;

    client.post(
        URL,
        {
            headers: fetch_headers(state)
        },
        (response) => {
            console.log(response);
        }
    );

};