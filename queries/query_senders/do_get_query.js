// Libraries
import { Client } from 'node-rest-client';

import action_creators from 'action_creators';

import fetch_headers from 'fetch_headers';

import global_vars_public from 'global_vars_public';

var client = new Client();

module.exports = (state, query) => {

    if (!state) throw new Error();
    if (!query) throw new Error();

    var time_sent = new Date();

    action_creators.REGISTER_QUERY_AS_SENT(query, time_sent);

    client.get(
        `${global_vars_public.API_ROOT}/${query.query_type}?query=${encodeURIComponent(JSON.stringify(query))}`,
        {
            headers: fetch_headers(state)
        },
        (response) => {
            if (response) {
                var time_received = new Date();
                action_creators.RECEIVE_QUERY(query, response['result'], time_received);
            }
            else {
                throw new Error();
            }
        }
    );

};