// Libraries
import { Client } from 'node-rest-client';

import action_creators from 'action_creators';
import do_get_query from 'do_get_query';

import fetch_headers from 'fetch_headers';
import get_queries from 'get_queries';

import global_vars_public from 'global_vars_public';

var client = new Client();

module.exports = (state, is_main_container, url_properties_of_url_str, field) => {

    if (!state) throw new Error();
    if (!is_main_container && !url_properties_of_url_str && !field) throw new Error();
    if (!state) throw new Error();

    var queries_to_be_sent;

    var do_queries = true;

    if (is_main_container) {

        queries_to_be_sent = get_queries.that_should_be_sent_for_main_container(state);

    }
    else if (field) {

        queries_to_be_sent = get_queries.that_should_be_sent_for_field(state, field);

    }
    else if (location) {

        var {pathname, search, hash} = location;

        queries_to_be_sent = get_queries.that_should_be_sent_for_url(state, url_properties_of_url_str);

    }

    if (queries_to_be_sent) {
        for (var query of queries_to_be_sent) {
            do_get_query(state, query);
        }
    }

};