// Libraries
import { Client } from 'node-rest-client';

import action_creators from 'action_creators';
import do_get_query from 'do_get_query';
import do_queries_for_container_or_field from 'do_queries_for_container_or_field';
import url_properties_from_location_or_url_str from 'url_properties_from_location_or_url_str';

import fetch_headers from 'fetch_headers';

import global_vars_public from 'global_vars_public';

// I had problems sending data in the data field, so as a temporary solution the form is
// sent as a query parameter instead. If the URLs of post requests are saved in the browser
// history or elsewhere this probably poses a security concern, though not a massive one.

var client = new Client();

module.exports = (state, form_obj, location) => {

    var form_obj_for_server = Object.assign({}, form_obj);

    var form_key = form_obj.form_key;

    if (state.forms_and_fields.key_of_current_dialog === form_key) {
        action_creators.CHANGE_KEY_OF_CURRENT_DIALOG(null, true);
    }

    action_creators.RESET_FORMS_AND_FIELDS();

    client.post(
        `${global_vars_public.API_ROOT}/post_form?data=${encodeURIComponent(JSON.stringify(form_obj))}`,
        {
            headers: fetch_headers(state)
        },
        (response) => {

            if (response['user_info']) {

                action_creators.UPDATE_TO_LOGGED_IN(response['user_info']);
                action_creators.CHANGE_KEY_OF_CURRENT_DIALOG(null, true);
                action_creators.RESET_FORMS_AND_FIELDS();

            }
            else if (response['success']) {

                if (location) {

                    do_queries_for_container_or_field(
                        state,
                        false,
                        url_properties_from_location_or_url_str(location),
                        null
                    );

                }

            }
            else {

                throw new Error();

            }

            if (form_obj.queries_on_response) {
                form_obj.queries_on_response.forEach(
                    x => do_get_query(state, x)
                );
            }

        }
    );

};