import btoa from 'btoa';

import global_vars_public from 'global_vars_public';

module.exports = (state) => {

    var return_obj = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Origin': global_vars_public.origin
    }
    
    var user_info = state.data.user_info;

    if (user_info) {
        var username = user_info.username;
        var login_cookie = user_info.login_cookie;
        var x = btoa(`${username}:${login_cookie}`);
        return_obj['Authorization'] = `Basic ${x}`;
    }

    return return_obj;
    
};