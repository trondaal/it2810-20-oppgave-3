var sha1 = require('sha1');
var password_hash_key = require('../../../global_vars_private.js').password_hash_key;

var models_and_model_settings = require('../../../models/models_and_model_settings.js');

var return_user_info = require('../helpers/return_user_info.js');
var check_username_and_password = require('../helpers/check_username_and_password.js');

var UserModel = models_and_model_settings['User'];

module.exports = (form, values_by_field_name, user, req, res) => {

    var fields = form.fields;

    var username = values_by_field_name['username'];
    var image = values_by_field_name['image'];
    var password = values_by_field_name['password'];
    var email = values_by_field_name['email'];

    var is_admin = false;

    if (!username && !password) res.send('Username and password missing');
    if (!username) res.send('Username missing');
    if (!password) res.send('Password missing');

    username = `${username.charAt(0).toUpperCase()}${username.substring(1)}`;

    var usernames_that_are_made_admins = ['Tor', 'Endelv', 'Endre', 'Herligetider', 'Lauritz', 'Mathiasjor', 'Mathias', 'Mikail', 'Vildema', 'Vilde'];

    if (usernames_that_are_made_admins.find(x => x === username)) {
        is_admin = true;
    }

    UserModel.findOne({ username: username }).exec(

        (error, user) => {

            if (user) {

                check_username_and_password(user, username, password, req, res, true);

            }
            else {

                var hashed_password = sha1(`${password}${password_hash_key}`);

                var new_user = UserModel();
                new_user['hashed_password'] = hashed_password;
                new_user['username'] = username;
                new_user['email'] = email;
                new_user['image'] = image;
                new_user['is_admin'] = is_admin;

                var date = new Date();

                var login_cookie = sha1(`${hashed_password}${date.toString()}`);

                new_user['login_cookies'] = [login_cookie];

                var dates_login_cookies_were_added = {};
                dates_login_cookies_were_added[login_cookie] = date;

                new_user['dates_login_cookies_were_added'] = dates_login_cookies_were_added;

                new_user.save(
                    (error) => {
                        if (error) console.log(error)
                    }
                );

                return_user_info(new_user, login_cookie, req, res);

            }

        }

    );

};