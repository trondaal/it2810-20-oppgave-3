// Helpers
var convert_mongoose_obj = require('../helpers/convert_mongoose_obj.js');
var autocomplete_field = require('../../../queries/query_receivers/helpers/autocomplete_field.js');

// Models
var models_and_model_settings = require('../../../models/models_and_model_settings.js');
var Book_model = models_and_model_settings['Book'];
var Transferral_request_model = models_and_model_settings['Transferral_request'];


var find_or_create_entity = settings => resolve_obj => {

    return new Promise(

        (resolve, reject) => {

            var search_for_entity = settings.search_for_entity;
            var model = models_and_model_settings[settings.model];
            var query = settings.query;
            var key_of_entity_to_fetch_or_create = settings.key_of_entity_to_fetch_or_create;

            if (typeof key_of_entity_to_fetch_or_create !== 'string') throw new Error(`the value of function_that_returns_query is ${function_that_returns_query}, but should be a non-empty string`);
            if (!model) throw new Error(`model not provided to find_or_create_entity-function for ${key_of_entity_to_fetch_or_create}`);
            if (typeof query !== 'object' && typeof query !== 'function') throw new Error(`the value of function_that_returns_query is ${function_that_returns_query}, but should be an object or a function`);

            if (typeof search_for_entity === 'function') search_for_entity = search_for_entity(resolve_obj);
            if (typeof query !== 'object') query = query(resolve_obj);

            if (typeof query !== 'object' && query !== false) throw new Error(`(typeof query !== 'object')`);

            if ((!query || typeof query !== 'object') && query !== false) throw new Error(`Something seems the be with the value for query, which is: ${query}`);

            if (!resolve_obj.was_retrieved) resolve_obj.was_retrieved = {};
            if (!resolve_obj.was_created) resolve_obj.was_created = {};
            if (!resolve_obj.did_search) resolve_obj.did_search = {};

            if (search_for_entity && query === false) throw new Error(`Either search_for_entity or query has ended up with the wrong value`);

            if (search_for_entity) {

                model.findOne(query).exec(
                    (error, result) => {
                        if (result) {

                            resolve_obj[key_of_entity_to_fetch_or_create] = result;
                            resolve_obj['was_retrieved'][key_of_entity_to_fetch_or_create] = true;
                            resolve_obj['was_created'][key_of_entity_to_fetch_or_create] = false;
                            resolve_obj['did_search'][key_of_entity_to_fetch_or_create] = true;
                            resolve(resolve_obj);

                        }
                        else {

                            resolve_obj[key_of_entity_to_fetch_or_create] = model();
                            resolve_obj['was_created'][key_of_entity_to_fetch_or_create] = true;
                            resolve_obj['was_retrieved'][key_of_entity_to_fetch_or_create] = false;
                            resolve_obj['did_search'][key_of_entity_to_fetch_or_create] = true;
                            resolve(resolve_obj);

                        }
                    }
                );

            }
            else {

                resolve_obj[key_of_entity_to_fetch_or_create] = model();
                resolve_obj['was_created'][key_of_entity_to_fetch_or_create] = true;
                resolve_obj['was_retrieved'][key_of_entity_to_fetch_or_create] = false;
                resolve_obj['did_search'][key_of_entity_to_fetch_or_create] = false;
                resolve(resolve_obj);

            }

        }
    );
};


var update_and_save_entity = settings => resolve_obj => {
    return new Promise(
        (resolve, reject) => {

            var key_of_entity_to_update = settings.key_of_entity_to_update;
            var model = models_and_model_settings[settings.model];
            var fields_to_update = settings.fields_to_update;

            if (typeof key_of_entity_to_update !== 'string') throw new Error(`Problem with the value of key_of_entity_to_update, which is: ${key_of_entity_to_update}`);
            if (!model) throw new Error(`model is missing`);
            if (!fields_to_update || typeof fields_to_update !== 'object') throw new Error(`Problem with the value of fields_to_update, which is: ${fields_to_update}`);

            var entity_to_update = resolve_obj[key_of_entity_to_update];

            if (!entity_to_update) throw new Error(`entity_to_update could not be found using key_of_entity_to_update ${key_of_entity_to_update}`);

            Object.keys(fields_to_update).forEach(
                key => {

                    var field_settings = fields_to_update[key];
                    var value = field_settings['value'];

                    if (typeof value === 'function') value = value(resolve_obj);

                    if (value) {

                        entity_to_update[key] = value;

                    }

                }
            )

            entity_to_update.save(
                (error, result) => {

                    if (error) {
                        console.log(error);
                        throw new Error(error);
                    }
                    else {
                        resolve_obj[key_of_entity_to_update] = result;
                        resolve(resolve_obj);
                    }

                }
            );

        }
    );
};


module.exports = (form, values_by_field_name, user, req, res) => {

    // Vars for book
    var title = values_by_field_name['title'];
    var isbn = values_by_field_name['isbn'];
    var book_id = values_by_field_name['book_id'];
    var image = values_by_field_name['image'];

    title = `${title.charAt(0).toUpperCase()}${title.substring(1)}`;

    // Vars for transferral_request

    var buy_or_sell = values_by_field_name['buy_or_sell'];

    // Query settings

    var settings_for_find_or_create_book = {

        model: 'Book',

        key_of_entity_to_fetch_or_create: 'book',

        search_for_entity: isbn || book_id,

        query: () => {

            if (isbn && book_id) {
                return {
                    '$or': [
                        { isbn: isbn },
                        { _id: book_id },
                    ]
                }
            }
            else if (isbn) {
                return { isbn: isbn }
            }
            else if (book_id) {
                return { _id: book_id }
            }
            else {
                return false;
            }

        }

    };

    var settings_for_find_or_create_transferral_request = {
        model: 'Transferral_request',
        key_of_entity_to_fetch_or_create: 'transferral_request',
        search_for_entity: (resolve_obj) => {
            return resolve_obj.was_retrieved.book && resolve_obj.user._id;
        },
        query: (resolve_obj) => {
            return {
                user_ref: resolve_obj.user._id,
                book_ref: resolve_obj.book._id
            }
        }
    };

    // Update settings

    var settings_for_update_and_save_book = {
        key_of_entity_to_update: 'book',
        model: 'Book',
        fields_to_update: {
            title: {
                value: title
            },
            title__autocomplete: {
                value: autocomplete_field(title, true)
            },
            image: {
                value: image
            },
            isbn: {
                value: isbn
            }
        }
    };

    var settings_for_update_and_save_transferral_request = {
        key_of_entity_to_update: 'transferral_request',
        model: 'Transferral_request',
        fields_to_update: {
            buy_or_sell: {
                value: buy_or_sell

            },
            user: {
                value: resolve_obj => {
                    return convert_mongoose_obj('User', resolve_obj.user);
                }
            },
            user_ref: {
                value: resolve_obj => {
                    return resolve_obj.user._id;
                }
            },
            book: {
                value: resolve_obj => {
                    return convert_mongoose_obj('Book', resolve_obj.book);
                }
            },
            book_ref: {
                value: resolve_obj => {
                    return resolve_obj.book._id;
                }
            },
            book_ref__by_isbn: {
                value: resolve_obj => {
                    return resolve_obj.book.isbn;
                }
            }
        }
    };

    var return_request_function = (resolve_obj, b) => {

        res.json({
            success: true,
            resolve_obj: resolve_obj,
            obj: convert_mongoose_obj(
                'Transferral_request',
                resolve_obj['transferral_request']
            )
        });
    };

    find_or_create_entity(settings_for_find_or_create_book)({ user: user })
        .then(update_and_save_entity(settings_for_update_and_save_book))
        .then(find_or_create_entity(settings_for_find_or_create_transferral_request))
        .then(update_and_save_entity(settings_for_update_and_save_transferral_request))
        .then(return_request_function)
        .catch((error) => console.log(error));

};