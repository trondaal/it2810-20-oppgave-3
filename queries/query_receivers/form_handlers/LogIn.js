var sha1 = require('sha1');
var password_hash_key = require('../../../global_vars_private.js').password_hash_key;

var models_and_model_settings = require('../../../models/models_and_model_settings.js');

var return_user_info = require('../helpers/return_user_info.js');
var check_username_and_password = require('../helpers/check_username_and_password.js');

var UserModel = models_and_model_settings['User'];

module.exports = (form, values_by_field_name, user, req, res) => {

    var fields = form.fields;

    var username = values_by_field_name['username'];
    username = `${username.charAt(0).toUpperCase()}${username.substring(1)}`;
    var password = values_by_field_name['password'];

    UserModel.findOne({ username: username }).exec(

        (error, user) => {

            if (user && user.hashed_password) {

                check_username_and_password(user, username, password, req, res);

            }
            else {
                res.json({ message: `There is no user with the username ${username}` });
            }

        }

    );

};

