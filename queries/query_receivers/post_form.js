// Form_handlers
var LogIn = require('./form_handlers/LogIn.js');
var Register = require('./form_handlers/Register.js');
var Book = require('./form_handlers/Book.js');

module.exports = (req, res) => {

      var user = req.user;

      var form = JSON.parse(req.query.data);

      var values_by_field_name = {};

      form.fields.forEach(
            (field) => values_by_field_name[field.field_name] = field.value
      );

      if (typeof form.form_type !== 'string') throw new Error();

      switch (form.form_type) {

        case 'LogIn':
            LogIn(form, values_by_field_name, user, req, res);
            break;
        case 'Register':
            Register(form, values_by_field_name, user, req, res);
            break;
        case 'Book':
            Book(form, values_by_field_name, user, req, res);
            break;
        default:
            throw new Error();

    }

};