var lodash = require('lodash');
var bluebird = require("bluebird");

// Helpers
var convert_mongoose_obj = require('./helpers/convert_mongoose_obj.js');

// From here: http://stackoverflow.com/questions/11985228/mongodb-node-check-if-objectid-is-valid
function is_valid_object_ID(str) {
    str = str + '';
    var len = str.length;
    var valid = false;
    if (len == 12 || len == 24) {
        valid = /^[0-9a-fA-F]+$/.test(str);
    }
    return valid;
}

var models_and_model_settings = require('../../models/models_and_model_settings.js');

var user_retrieved_for_other_object;
var book_retrieved_for_other_object;


module.exports = (req, res) => {

    var query = JSON.parse(req.query.query);

    var find_several_entities = query.find_several_entities;
    var find_one_entity = query.find_one_entity;
    var count = query.count;

    var entity_type = query.entity_type;
    var slug = query.slug;
    var parameters = query.parameters;
    var limit = query.limit;
    var skip = query.skip;

    if (!find_several_entities && !find_one_entity && !count) throw new Error();
    if ([find_several_entities, find_one_entity, count].filter.length > 1) throw new Error();
    if (!entity_type) throw new Error();
    if (entity_type !== 'Book' && entity_type !== 'Transferral_request') throw new Error(`You have set entity_type to ${entity_type}. Is that correct?`);

    var Entity_model = models_and_model_settings[entity_type];

    var and_arr = [];
    var main_query = {};

    if (slug) {

        if (entity_type === 'Book') {
            if (is_valid_object_ID(slug)) {
                main_query['_id'] = slug;
            }
            else {
                main_query['isbn'] = slug;
            }
        }

    }

    if (parameters) { //state.forms_and_fields.search_bar_value

        Object.keys(parameters).forEach(
            (key) => {

                if (parameters[key] !== null && parameters[key] !== undefined) {

                    if (key === 'book_slug') {

                        if (typeof parameters[key] !== 'string') throw new Error();

                        if (find_several_entities || count) {
                            if (is_valid_object_ID(parameters[key])) {
                                main_query['book_ref'] = parameters[key];
                            }
                            else {
                                main_query['book_ref__by_isbn'] = parameters[key];
                            }
                        }
                        else if (find_one_entity) {
                            if (is_valid_object_ID(parameters[key])) {
                                main_query['_id'] = parameters[key];
                            }
                            else {
                                main_query['isbn'] = parameters[key];
                            }
                        }

                    }
                    else if (key === 'buy_or_sell') {

                        throw new Error("else if (key === 'buy_or_sell') {");

                    }
                    else if (key === 'title__autocomplete') {

                        if (typeof parameters[key] !== 'string') throw new Error();
                        main_query['title__autocomplete'] = parameters[key].toLowerCase();

                    }
                    else if (key === 'title__text_search') {

                        if (typeof parameters[key] !== 'string') throw new Error();

                        and_arr.push(
                            { $text: { $search: parameters[key] } }
                        )

                    }
                    else if (key === 'user_ref') {

                        if (typeof parameters[key] !== 'string') throw new Error();
                        main_query['user_ref'] = parameters[key]

                    }
                    else {

                        throw new Error(
                            `The key ${key} is not accounted for.
                        find_several_entities set to ${find_several_entities}.
                        find_one_entity set to find_one_entity set to ${find_one_entity}.
                        entity_type set to find_one_entity set to ${entity_type}.`
                        );

                    }

                }

            }
        );

    }

    var query_for_db;

    if (and_arr.length === 0) {

        query_for_db = main_query;

    }
    else if (and_arr.length === 1) {

        if (lodash.isEqual(main_query, {})) {
            query_for_db = and_arr[0];
        }
        else {
            query_for_db = and_arr.concat([main_query]);
        }

    }
    else {

        if (lodash.isEqual(main_query, {})) {
            query_for_db = and_arr;
        }
        else {
            query_for_db = and_arr.concat([main_query]);
        }

    }

    var handle_result = (error, result, y) => {

        if (error) {

            console.log(error);

        }

        if (result) {

            if (entity_type === 'Transferral_request' && find_several_entities) {

                var return_value = result.map(
                    x => convert_mongoose_obj('Transferral_request', x, false)
                );

                res.json({ result: return_value });

            }
            else if (entity_type === 'Book' && find_several_entities) {

                var return_value = result.map(
                    x => convert_mongoose_obj('Book', x, false)
                );

                res.json({ result: return_value });

            }
            else if (entity_type === 'Book' && find_one_entity) {

                var return_value = convert_mongoose_obj('Book', result, false);

                res.json({ result: return_value });

            }
            else if (count) {

                res.json({ result: result });

            }
            else {

                throw new Error();

            }

        }

    }

    if (find_one_entity) {
        query_function = Entity_model.findOne(query_for_db).exec(handle_result);
    }
    else if (find_several_entities) {
        var query_function = Entity_model.find(query_for_db);
        if (limit) query_function = query_function.limit(limit);
        if (skip) query_function = query_function.skip(skip);
        query_function.exec(handle_result);
    }
    else if (count) {
        Entity_model.where(query_for_db).count(handle_result);
    }
    else {
        throw new Error();
    }

};