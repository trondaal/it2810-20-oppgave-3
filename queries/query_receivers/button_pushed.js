var models_and_model_settings = require('../../models/models_and_model_settings.js');

module.exports = (req, res) => {

    var button_type = req.query.button_type;
    var entity_type = req.query.entity_type;
    var entity_id = req.query.entity_id;

    if (!button_type) throw new Error();
    if (!entity_type) throw new Error();
    if (!entity_id) throw new Error();

    if (button_type === 'delete_entity') {

        var Model = models_and_model_settings[entity_type];
        var Model_settings = models_and_model_settings.model_settings[entity_type];

        Model.findOneAndRemove({ _id: entity_id }).exec(
            error => {
                if (error) {
                    console.log(error);
                    res.json({success: false});
                }
                else {
                    res.json({success: true});
                }
            }
        );

    }
    else {

        throw new Error();
        
    }

};