var sha1 = require('sha1');
var password_hash_key = require('../../../global_vars_private.js').password_hash_key;

var return_user_info = require('./return_user_info.js');

module.exports = (user, username, password, req, res, from_register_form) => {

    if (sha1(`${password}${password_hash_key}`) === user.hashed_password) {

        var login_cookies = user['login_cookies'];
        var dates_login_cookies_were_added = user['dates_login_cookies_were_added'];
        var hashed_password = user.hashed_password;

        if (!login_cookies) throw new Error();
        if (!dates_login_cookies_were_added) throw new Error();

        var date = new Date();

        var new_login_cookie = sha1(`${hashed_password}${date.toString()}`);
        login_cookies.push(new_login_cookie);
        dates_login_cookies_were_added[new_login_cookie] = date;

        user['login_cookies'] = login_cookies;
        user['dates_login_cookies_were_added'] = dates_login_cookies_were_added;

        user.save(
            (error) => {
                if (error) console.log(error)
            }
        );

        return_user_info(user, new_login_cookie, req, res);

    }
    else {

        if (from_register_form) res.json({ message: `A user with this username already exists` });
        else res.json({ message: `Wrong username or password` });

    }

};