var model_settings = require('../../../models/models_and_model_settings.js').model_settings;

module.exports = (obj_type, mongoose_obj) => {

    if (!obj_type) throw new Error('obj_type missing');
    if (!mongoose_obj) throw new Error('mongoose_obj missing');

    var keys = Object.keys(mongoose_obj.toObject()).filter(
        key => key !== '__v' && key !== '_id'
    );

    var return_obj = {
        _id: mongoose_obj['_id']
    };

    keys.forEach(
        (key) => {
            
            var display_setting = model_settings[obj_type][key].display_setting;

            if (!display_setting) {
                throw new Error(`The key ${key} seems to not have the setting display_setting`)
            }

            if (key === '_id') {
                return_obj[key] = mongoose_obj[key];
            }
            else if (display_setting === 'display_by_default') {
                return_obj[key] = mongoose_obj[key];
            }
            else if (display_setting !== 'hide_by_default') {
                throw new Error("else if (display_setting !== 'display_by_default'){");
            }

        }
    );

    return return_obj;

};