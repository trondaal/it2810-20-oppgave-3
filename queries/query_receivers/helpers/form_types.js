// If this file throws an error after having worked previously, it may be because
// it (or the files containing form_types) has been moved or resides in one or more
// folders that have had their names changed 

var fs = require('fs');

// Will break in the case of some types of changes to the placements of folders or files
var dirform_types = __dirname.replace('/queries/query_receivers/helpers', '/form_types');

form_types = {};

// Makes so that the different form_types are stored in an object where they are keyed by their names
// fs.readdir is asynchronous, so post_form.js may not work for some fraction of a second after it has been started
fs.readdir(
      dirform_types,
      (err, filenames) => {
            if (!filenames) throw new Error();
            for (var filename of filenames) {
                  var modified_filename = filename.replace('.js', '');
                  form_types[modified_filename] = require(`${dirform_types}/${modified_filename}.js`);
            }
      }
);

module.exports = form_types;