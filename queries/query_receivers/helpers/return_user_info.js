module.exports = (user, new_login_cookie, req, res) => {

    res.json({
        user_info: {
            image: user.image,
            username: user.username,
            login_cookie: new_login_cookie,
            is_admin: user.is_admin,
            _id: user._id,
        }
    })

};