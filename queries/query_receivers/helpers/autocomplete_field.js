var autocomplete_field_generator_helper = (str, min_length) => {

    if (str.length < min_length) {
        return [];
    }

    var return_arr = [];

    for (var i = 0; i < str.length - min_length + 1; i++) {
        
        return_arr.push(
            str.substring(0, str.length - i)
        );

    }

    return return_arr;

};

module.exports = (str, split_words) => {

    var str = str.toLowerCase();

    var min_length = 1;

    if (split_words) {

        var a = str.split(' ');
        var b = a.map((x, index) => a.slice(index));
        var c = [];
        b.forEach(
            arr => c.push(arr.join(' '))
        );
        var d = c.map(
            x => autocomplete_field_generator_helper(x, min_length)
        );
        
        var return_val = [];

        d.forEach(
            x => x.forEach(
                y => return_val.push(y)
            )
        );

        if (return_val.length === 0) return null;
        else return return_val;

    }
    else {

        return autocomplete_field_generator_helper(str, min_length);

    }


};